# This is the start of what I sent in the email -- Jared

- We established that email is best way to communicate
- Web site navigation is a concern
  - Menu was one of the biggest problems in the website design
  - Lots of scrolling and lack of dynamic content also hurts
- There are a lot of limitations to the web site currently
  - Website templates are hardcoded
  - Purchasing should ideally done without a third party
  - Adding new content or modifying how pages look is difficult

Action items we will look into:
- Updating wordpress to the latest version
- Create a QA testing site for them so they can try things out without breaking the real site.
- Work on the new version of the site and make it available at a preview URL
- Fleshing out the requirements for the new site further

Action items we need from you:
- Examples of websites you really like in terms of design
- The database dump
- The website codebase



# Notes from the meeting for people to refer back to

- web site not completely broken: user can buy tickets, and donate
- web site is not dynamic: need motion, moment. It’s very boring, no ability to go and change things,
- people spend little time navigating since they get tired of all the scrolling. You have to scroll up and header is problematic. Scroll across or down to figure out where things are.
- donation works fine, but menu has many headers and you don't know where to go. User have hard time finding what they are looking for. Is it under community, etc...user click ....click ...click
- we can't add to this list (difficult to add...again limitations")
- Portand Opera has a lot of events (a lot going on) but they are limited to one sort of action.

-Question: how you want project to happened?
Answer: They asked for recommendations from us.
WordPress what will happen in two years,
content management system: it's important to make a page and make an update.
Web site was developed by a company using templates that are hard coded. They made a template page hard codded and they ran out of business. it’s difficult to make small changes. They have the Source code and it’is available along with db that is stored with 3rd party. Not in their server.

-Flip side throw a map that doesn't work and start with another one.

-Recap: you want much more flexible and more connected web site to make it much easier to find things.
-start from scratch,
-client wants to add things
- they mentioned that their web site serves external (production artistic and broadway) and external. This web site stays the way it's and the other one will replace it. it's win win.
-the entire old web site will also will stay.

-Time issue: we have until 8/27

- client don’t want to push too quickly. They prefer WordPress, just changes to it.
- they want protect the integrity of their web site; we will use one copy locally and then move the other copy over.

- We need to upgrade the WordPress version. They said they couldn’t do the update on their own- self populated, get the site to work in an updated version of WordPress.

Main issues:
-update, then go to testing and
- make huge impact by fixing scrolling
- donation page is fine, but they want to get rid of group party for donation
- Functionality steady to make changes without breaking
- Navigation is not good
- they need dynamic content

- no need to check with us
- difficult to hold a meeting for them
- email will be hopeful
Good URL in term of design and naviagation
Nagivation: oregonsyphony.org
Calendar:
theatre.depaul.edu theater school

gardenmusum.org

- dynamic and music, beautiful we sites

- visitor of the web site experiences movement of art and hears song and visual stage

- performance of art is important to them as an organization. It set them apart and make them feel dynamic

- Meeting depending on schedule
- we will put demo version out there and so client can see what's like as we work on it.

For our team, we will perform web site development using the following guidelines:
- better design
- get rid of things they don't like
- use wordpress
- have a demo web site ready for the client
- they will send us urls of web sites they like
- they will give us acces to database and the source code
