# Switchyard Wordpress Fabric Config file
# This keeps the settings separate from the fabric commands

import os

from fabric.api import env, local, sudo, run

"""
Base configuration
"""
env.settings = "localhost"
env.project_name = "portland-opera"
env.path = "%s" % os.getcwd()
env.data_path = "%s/data" % os.getcwd()
env.local_path = os.getcwd()

# If you said svn, where should I checkout from?
env.wpdomain = 'opera.dev'
env.strategy = 'git'

# If you said git, where should I clone from and which branch should I checkout?
env.gitrepo = 'git@rallygroup.beanstalkapp.com:portland-opera.git'
env.gitbranch = 'master'

# Do we want to automatically deploy, default: false
env.automatic_deploy = False

# These are the credentials for the wordpress. They should match your wp-config.php.
env.db_host = 'localhost'
env.db_name = 'opera'
env.db_port = 3306
env.wp_user_name = 'admin'
env.wp_user_pass = 'opera'
env.db_wpuser_name = 'opera'
env.db_wpuser_pass = 'opera'  # make up something complicated for the password

# Super user name and pass for adding users and databases to mysql
env.db_root_user = "root"
env.db_root_pass = "root"

# gunicorn
env.gunicorn = 'gunicorn'


# This is the config file that will get installed on bootstrap
env.config_file = 'wp-config-fabric.php'

# Fix permissions throughout the deployment process. You may need to use this
# if perms are getting messed up.
env.fix_perms = False
env.flush_rewrites = True

# This defaults the run and sudo functions to local, so we don't have to duplicate
# code for local development and deployed servers.
env.sudo = local
env.run = local

# Where should I get Wordpress??
env.wp_tarball = "http://wordpress.org/latest.tar.gz"

env.basecamp_project = 0


"""
Environments
"""
def production():
    """
    Work on production environment
    """
    env.settings = 'production'
    env.hosts = ['216.151.11.74']
    env.port = 2244
    env.user = 'portlandop'
    env.path = '/home/portlandop/domains/portlandopera.org'
    env.docroot_path = '/home/portlandop/domains/portlandopera.org/www'
    env.wpdomain = 'www.portlandopera.org'
    env.db_root_user = 'portlandop_wp'
    env.db_root_pass = 'apH1ciS1hYeps2Ig9og5'
    env.db_user = 'portlandop_wp'
    env.db_wpuser_name = 'portlandop_wp'
    env.db_wpuser_pass = 'apH1ciS1hYeps2Ig9og5'
    env.db_host = 'localhost'
    env.db_name = 'portlandop_wp'
    env.gitbranch = 'production'
    env.data_path = '/home/portlandop/domains/portlandopera.org/data'
    env.apache_group = 'apache'
    env.apache_user = 'apache'
    env.automatic_deploy = False
    env.sudo = sudo
    env.run = run



def beta():
    """
    Work on beta environment
    """
    env.settings = 'beta'
    env.hosts = ['beta.opera.sysite.co']
    env.user = 'wordpress'
    env.sudo_user = 'root'
    env.path = '/data/sites/beta.opera.sysite.co'
    env.docroot_path = '/data/sites/beta.opera.sysite.co/www'
    env.wpdomain = 'beta.opera.sysite.co'
    env.wp_user_name = 'opera-admin'
    env.wp_user_pass = ''
    env.db_root_user = 'wordpress'
    env.db_root_pass = 'SWqWj6dTRb'
    env.db_host = 'localhost'
    env.db_name = 'beta-opera'
    env.apache_group = 'wordpress'
    env.apache_user = 'www-data'
    env.data_path = '/data/sites/beta.opera.sysite.co/data'
    env.gitbranch = 'master'
    env.config_file = 'wp-config-fabric.php'
    env.automatic_deploy = True
    env.fix_perms = False
    env.flush_rewrites = True
    env.sudo = sudo
    env.run = run


def staging():
    """
    Work on staging environment
    """
    env.settings = 'staging'
    env.hosts = ['opera.sysite.co']
    env.user = 'wordpress'
    env.password = 'fFz3ayWmYt'
    env.path = '/data/sites/opera.sysite.co'
    env.docroot_path = '/data/sites/opera.sysite.co/www'
    env.wpdomain = 'opera.sysite.co'
    env.wp_user_name = 'opera-admin'
    env.wp_user_pass = 'Bif4Thi3hiv8Uv4deD3C'
    env.db_root_user = 'wordpress'
    env.db_root_pass = 'SWqWj6dTRb'
    env.db_host = 'localhost'
    env.db_name = 'opera'
    env.apache_group = 'www-data'
    env.apache_user = 'wordpress'
    env.data_path = '/data/sites/opera.sysite.co/data'
    env.gitbranch = 'master'
    env.config_file = 'wp-config-fabric.php'
    env.automatic_deploy = True
    env.fix_perms = False
    env.flush_rewrites = True
    env.sudo = sudo
    env.run = run

try:
    from local_config import *
except:
    pass
