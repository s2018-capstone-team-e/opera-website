<?php

get_header(); ?>

<!-- Begin Page -->
<?php get_template_part('partial-hero'); ?>

<?php get_template_part('partial-breadcrumbs'); ?>


<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-10 medium-centered columns">
            <h1><?php the_title(); ?> </h1>

            <p class="intro"><?php the_field("intro"); ?></p>
        </div>
    </div>
</section>

<?php get_template_part("partial", $post->post_name); ?>

<div class="row">
    <div class="medium-8 columns entry-content">

        <?php the_content(); ?>

    </div>
</div>


<!-- End Page -->

<?php get_footer(); ?>