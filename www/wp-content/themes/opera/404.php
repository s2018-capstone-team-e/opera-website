<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 */

get_header(); ?>
<section id="introduction" class="section introduction has-rule">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1>Page not found.</h1>
            <p class="intro">We're sorry to say, we could not find the page you were looking for.</p>
        </div>
    </div>

</section>

<div class="row">

    <div class="medium-8 medium-centered columns">

        <article class="entry-content">
            <p class="large">Here's some options:</p>
            <ol>
                <li>Return to the <a href="/">homepage</a>.</li>
                <li>Go <a href="javascript:history.back()">back</a> to the previous page.</li>
                <li>Check out our current <a href="/operas">season lineup</a>!</li>
            </ol>

        </article>

    </div>

</div>

<?php get_footer(); ?>