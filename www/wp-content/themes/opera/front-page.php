<?php get_header(); ?>

<!-- Begin Page -->

<?php if (get_field('notification_text', 'option')): ?>
    <!-- Optional Notification Item -->
    <div data-alert class="alert-box news-notice">
        <div class="message">
            <p><span class="icon"></span><a
                        href="<?php the_field('notification_url', 'option'); ?>"><?php the_field('notification_text', 'option'); ?></a>
            </p>
        </div>

        <a href="#" class="close">&times;</a>
    </div>
<?php endif; ?>

<section id="hero" class="home-hero has-image has-cta">

    <?php $images = get_field('background_image');

    if ($images): ?>
        <div id="carousel" class="flexslider">
            <ul class="slides">
                <?php foreach ($images as $image): ?>
                    <li>
                        <?php if(isset($image['sizes'])) { ?>
				<img src="<?= $image['sizes']['large'] ?>" alt="<?= $image['alt'] ?>" />
                        <?php } else { ?>
                        <video loop controls autoplay muted preload="auto" data-image="<?= $image['icon'] ?>" style="object-fit: cover; width: 100%;" src="<?= $image['url'] ?>"></video> <?php 
                        } ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php if(get_field("cta_text")): ?>
        <div class="home-cta">
            <div class="row">
                <div class="medium-12 columns">
            			<p><?php the_field("cta_text"); ?></p>
                    <a href="<?php the_field("cta_link"); ?>" class="btn-cta"><?php the_field("cta_link_text"); ?> &raquo;</a>
                </div>
            </div>
        </div>
        <?php endif; ?>
    <?php endif; ?>
</section>


<section id="season-lineup" class="home-opera-lineup section">
    <?php $post_objects = get_field('section_1_productions');

    if ($post_objects): ?>
        <div class="row">
            <div class="medium-12 columns">
                <h2 class="has-rule"><?php the_field('section_1_title') ?> <a href="/operas" class="btn-more">View All
                        &raquo;</a></h2>
            </div>
        </div>
        <div class="row">
            <div class="medium-12 columns">

                <ul id= "season-grid-template" class="production-count medium-block-grid-3">

                    <?php foreach ($post_objects as $post): // variable must be called $post (IMPORTANT)?>
                        <?php setup_postdata($post); ?>
                        <li class="production-obj is-opera">
                            <div class="image-wrapper">
                                <a href="<?php the_permalink(); ?>" class="thumbnail">

                                    <?php if ($image = get_field('detail_image')): ?>
                                        <img src="<?php echo $image['sizes']['season']; ?>">
                                    <?php endif; ?>
                                </a>

                                <div class="details">
                                    <a href="<?php the_permalink(); ?>" class="view-more">View More &raquo;</a>
                                </div>
                            </div>

                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly?>

            </div>
        </div>
    <?php endif; ?>
</section>

<?php if (get_field('show_section_2')) : ?>
    <section id="season-lineup-2" class="home-opera-lineup section">
        <?php $post_objects = get_field('section_2_productions');

        if ($post_objects): ?>
            <div class="row">
                <div class="medium-12 columns">
                    <h2 class="has-rule"><?php the_field('section_2_title') ?> <a href="/operas" class="btn-more">View
                            All &raquo;</a></h2>
                </div>
            </div>
            <div class="row">
                <div class="medium-12 columns">

                    <ul  class="production-count medium-block-grid-3">

                        <?php foreach ($post_objects as $post): // variable must be called $post (IMPORTANT)?>
                            <?php setup_postdata($post); ?>
                            <li class="production-obj is-opera">
                                <div class="image-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="thumbnail">

                                        <?php if ($image = get_field('detail_image')): ?>
                                            <img src="<?php echo $image['sizes']['season']; ?>">
                                        <?php endif; ?>

                                    </a>

                                    <div class="details">
                                        <a href="<?php the_permalink(); ?>" class="view-more">View More &raquo;</a>
                                    </div>
                                </div>

                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly?>

                </div>
            </div>
        <?php endif; ?>
    </section>
<?php endif; ?>
<section class="section2 love-and-support ">
    <section id="support-po" class="support-po has-intro">
        <div class="row">
            <div class="medium-10 medium-centered columns">
                <div class="intro">
                    <h2>Support the Opera</h2>
                    <p><?php the_field('support_content'); ?></p>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="medium-10 medium-centered columns">
                <p class="caption "><?php the_field('image_quotation'); ?><br>
                    <span><?php the_field('image_quotation_author'); ?></span></p>
                <div class="row">
                    <div class="medium-8 medium-centered columns support-po-copy">
                        <p><?php the_field('support_text'); ?></p>
                        <a href="<?php the_field("benefits_link"); ?>" class="benefits">Benefits of Giving &raquo;</a>
                        <a href="<?php the_field("give_now_link"); ?>" id= "donate-hover-button" class="button">Donate</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="opera-love" class="love-the-opera has-background">
        <div class="row">
            <div class="medium-12 columns">
                <h2>Opera <span class="icon" title="Loves">Loves</span> Portland</h2>
                <div class="row">

                    <?php $count = 1; ?>
                    <?php if (have_rows('main_content_area')): ?>
                        <?php while (have_rows('main_content_area')): the_row(); ?>
                            <article
                                    class="medium-4 columns opera-love-section <?php echo 'content-block-' . $count++; ?>">
                                <h3><?php the_sub_field('title'); ?></h3>
                                <p><?php the_sub_field('content'); ?></p>
                                <a href="<?php echo get_sub_field('link_override_url') ?: get_sub_field('linked_page'); ?>"
                                   class="more"><?php the_sub_field('link_text'); ?> &raquo;</a>
                            </article>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </section>


</section>


<!-- End Page -->

<?php get_footer(); ?>
