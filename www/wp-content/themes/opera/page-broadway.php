<?php

get_header(); ?>

<!-- Begin Page -->

<?php get_template_part('partial-hero'); ?>


<section id="introduction" class="section dark-on-light introduction">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <?php
                if ($title_image = get_field('title_image')):
                    echo $title_image;
                else:
            ?>
            <h1><?php the_title(); ?></h1>
            <?php endif; ?>
            <p class="intro"><?php the_field("intro"); ?></p>
            <?php if (get_field('season_tickets_link')): ?>
            <a href="<?php the_field("season_tickets_link"); ?>" class="button">Subscribe to this Season</a>
            <?php endif; ?>
        </div>
    </div>
</section>



<section id="season-lineup" class="broadway light-on-dark has-background">
    <div class="row">
        <div class="medium-12 columns">
            <h2 class="has-rule"><?php the_field('broadway_season', 'option') ?> </h2>

        </div>
    </div>
    <div class="row">
        <div class="medium-12 columns">

            <?php
                $args = array(
                    'post_type' => 'production',
                    'production-type' => 'broadway',
                    'orderby' => 'menu_order',
                    'order' => 'ASC',
                    'posts_per_page' => 20
                );
                $query = new WP_Query($args);
            ?>
            <?php if ($query->have_posts()): ?>
            <ul class="broadway-count medium-block-grid-4 small-block-grid-2">

                <?php while ($query->have_posts()): $query->the_post(); ?>
                <li class="production-obj is-broadway">
                    <a href="<?php the_permalink(); ?>" class="thumbnail">
                        <?php the_post_thumbnail('opera-small'); ?>
                    </a>
                    <div class="details<?php if (!get_field("tickets_url")): ?> no-tickets<?php endif; ?>">
                        <div class="details-upper">
                            <h3 class="production-title"><?php the_title(); ?></h3>
                            <?php if (hasProductionTimes(get_the_ID())): ?>
                            <p class="production-date"><?php echo getDateRange(get_the_ID()); ?></p>
                            <?php endif; ?>
                        </div>

                        <a href="<?php the_permalink(); ?>" class="view-more">View More &raquo;</a>
                        <?php if (get_field("tickets_url")): ?>
                        <a href="<?php the_field("tickets_url"); ?>" class="tickets"><span></span> Get Tickets &raquo;</a>
                        <?php endif; ?>
                    </div>
                </li>
                <?php endwhile; ?>

            </ul>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>

        </div>
    </div>
</section>


<section id="ticket-info" class="expandable-content has-intro ticket-info">
    <div class="panel">
        <div class="row">
            <div class="medium-12 columns">
                <h2><span class="icon"></span>Ticket Information</h2>
                <div class="intro will-fadeout">
                    <ul class="link-list is-inline">
                        <?php if (get_field('season_tickets_link')): ?>
                        <li><a href="<?php the_field("season_tickets_link"); ?>" target="_blank">Subscribe or Renew &raquo;</a></li><!-- Ticketmaster URL -->
                        <?php endif; ?>
                        <li><a href="<?php the_field("group_tickets_link"); ?>" target="_blank">Group Tickets &raquo;</a></li>
                        <!-- <li><a href="<?php the_field("individual_ticket_link"); ?>">Individual Tickets &raquo;</a></li> -->
                    </ul>
                </div>
            </div>

        </div>
    </div>

    <div class="inner-content">
        <section class="two-thirds left">
            <div class="inner-wrapper">
                <h2>Ticket Information</h2>
                <p><?php echo get_field('ticket_content'); ?></p>
            </div>

        </section>

        <section class="one-third right">
            <div class="inner-wrapper">
                <ul class="link-list">
                    <?php if (get_field('season_tickets_link')): ?>
                    <li><a href="<?php the_field("season_tickets_link"); ?>" target="_blank">Subscribe or Renew &raquo;</a></li><!-- Ticketmaster URL -->
                    <?php endif; ?>
                    <li><a href="<?php the_field("group_tickets_link"); ?>" target="_blank">Group Tickets &raquo;</a></li>
                </ul>
                <div class="individual-tickets-info">
                    <p>Individual tickets may be purchased by linking into Broadway performance details from the top of this page or directly from <a href="http://portland.broadway.com">Broadway in Portland</a>. Please call the Portland Opera <a href="/contact">front desk</a> during business hours for further questions: 503-241-1802.</p>
                </div>
            </div>
        </section>
        <section class="two-thirds left">
            <div class="inner-wrapper">
                <p><strong>The Hampton Opera Center</strong> <br>
                        <?php the_field('address', 'option'); ?>

                        <?php if (have_rows('phone_numbers', 'option')): $i = 0; ?>
                        <p>
                        <?php while (have_rows('phone_numbers', 'option')): the_row(); ?>
                        <?php the_sub_field('display_phone_number'); ?><?php echo($i == 0 ? " or " : ""); ?>
                        <?php $i++; endwhile; ?>
                        </p>
                        <?php endif; ?>

                        <p class="hours"><?php the_field('hours', 'option'); ?></p>
            </div>

        </section>
    </div>
</section>


<section id="support-po" class="section support-po has-intro">
    <div class="row">
        <div class="medium-10 medium-centered columns">
            <div class="intro">
                <h2>Support Broadway</h2>
                <p><?php the_field('support_excerpt'); ?></p>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="medium-10 medium-centered columns">
            <p class="caption"><?php the_field("support_quote"); ?> <br> <span><?php the_field("support_quote_author"); ?></span></p>
            <div class="row">
                <div class="medium-8 medium-centered columns support-po-copy">
                    <p><?php the_field('support_content'); ?></p>
                    <a href="<?php the_field('benefits_link'); ?>">Benefits of Giving &raquo;</a>
                    <a href="<?php the_field('give_now_link'); ?>" class="button">Give Now</a>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="repertory-list" class="section light-on-dark has-background repertory-list">
    <div class="row">
        <div class="medium-12 center"><a href="<?php the_field("repertory_link"); ?>" class="btn-cta">Broadway Repertory List &raquo;</a></div>
    </div>
</section>

<!-- End Page -->

<?php get_footer(); ?>
