<?php
/*

Template Name: Flexible Page Template

*/


get_header(); ?>

<!-- Begin Page -->

<?php get_template_part('partial-hero'); ?>

<?php get_template_part('partial-breadcrumbs'); ?>


<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-10 medium-centered columns">
            <h1><?php the_title(); ?> </h1>

            <p class="intro"><?php the_field("intro"); ?></p>
        </div>
    </div>
</section>

<?php get_template_part("partial", $post->post_name); ?>

<div class="row">
    <div class="medium-8 columns entry-content">

        <?php the_content(); ?>

    </div>

    <?php if (($events = get_field('event_widget')) || have_rows('sidebar_content')) : ?>

    <!-- Sidebar widgets -->

    <div class="medium-4 columns sidebar">

        <?php if (is_array($events)): ?>
        <!-- Events widget -->
        <div class="events-widget event-category">
            <h3>Upcoming Events</h3>
            <a href="<?php post_permalink(ID_by_slug('calendar')); ?>" class="calendar-link">Calendar</a>
            <ul class="event-list">
                <?php foreach ($events as $event): ?>
                    <li><span class="date"><?php echo date('m/d/Y', calendar_event_get_next_timestamp($event->ID)); ?></span> <a href="<?php echo post_permalink($event->ID); ?>" class="title"><?php echo get_the_title($event); ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>

        <?php endif; ?>

        <?php // Sidebar Flexible Content?>
        <?php if ($sidebars = have_rows('sidebar_content')): ?>

        <?php while (have_rows('sidebar_content')) : the_row(); ?>

            <div class="<?php echo str_replace('_', '-', get_row_layout()); ?> clearfix">
                <h4 class="has-rule"><?php the_sub_field('content_title') ?></h4>

                <?php if (get_row_layout() == 'related_content'): ?>
                <?php $links = get_sub_field('related_content_links'); ?>

                <ul>
                    <?php foreach ($links as $link): ?>
                    <li><a href="<?php echo post_permalink($link->ID);?>"><?php echo get_the_title($link); ?></a></li>
                    <?php endforeach; ?>
                </ul>

                <?php elseif (get_row_layout() == 'external_links'): ?>
                <?php $links = get_sub_field('external_links'); ?>

                <ul>
                    <?php foreach ($links as $link): ?>
                    <li><a target="_blank" href="<?php echo maybe_add_http($link['link_url']);?>"><?php echo $link['link_text']; ?></a></li>
                    <?php endforeach; ?>
                </ul>

                <?php elseif (get_row_layout() == 'static_content'): ?>

                <div><?php the_sub_field('static_content'); ?></div>

                <?php endif; ?>

            </div>


        <?php endwhile; ?>
        <?php endif; ?>

    </div>

    <?php endif; ?>
</div>

<?php // Custom Page Content?>
<?php if ($content_areas = have_rows('content_area')): ?>

<?php while (have_rows('content_area')) : the_row(); ?>
<section class="section content-section">
<div class="row content-area <?php echo sanitize_title(get_row_layout()); ?>">
    <div class="medium-12 columns">

            <?php if (get_row_layout() == 'major_title'): ?>

                <div class="text-<?php the_sub_field('paragraph_alignment');?>">

                <h2><?php the_sub_field('content_title'); ?></h2>

                </div>

            <?php elseif (get_row_layout() == 'grid'): ?>

                <h3><?php the_sub_field('content_title'); ?></h3>

                <ul class="medium-block-grid-3">
                    <?php while (have_rows('grid_items')) : the_row(); ?>
                    <li>  <!-- Typically three fields, at most -->
                        <h4><?php the_sub_field('item_title'); ?></h4> <!-- Heading (Name, Title) -->
                        <div><?php the_sub_field('item_content'); ?></div>
                    </li>
                    <?php endwhile; ?>
                </ul>


            <?php elseif (get_row_layout() == 'paragraph'): ?>

                <div class="text-<?php the_sub_field('paragraph_alignment');?>">

                <h3><?php the_sub_field('content_title'); ?></h3>

                <?php the_sub_field('paragraph_content'); ?>
                </div>

            <?php elseif (get_row_layout() == '2col_paragraph'): ?>


                <div class="row">
                    <div class="medium-6 columns">
                        <h3><?php the_sub_field('paragraph1_title'); ?></h3>

                        <?php the_sub_field('paragraph1_content'); ?>
                    </div>
                    <div class="medium-6 columns">
                        <h3><?php the_sub_field('paragraph2_title'); ?></h3>

                        <?php the_sub_field('paragraph2_content'); ?>
                    </div>
                </div>

            <?php endif; ?>


    </div>
</div>
</section>

<?php endwhile; ?>

<?php endif; ?>


        <!-- End Page -->

<?php get_footer(); ?>