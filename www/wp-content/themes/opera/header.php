<!DOCTYPE html>
<html class="no-js">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>
    <?php wp_title(); ?>
  </title>
  <meta name="viewport" content="width=device-width">

  <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <header role="banner">

    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1>
            <a href="<?= get_site_url() ?>">
              <?php bloginfo('name'); ?>
            </a>
          </h1>
        </li>
      </ul>
      <ul id="main_nav" class="navigation"> <?php
        $menu_links = strip_tags(wp_nav_menu(
          array('echo' => false,
                'menu' => 'Main Menu',
                'menu_class' => 'menu',
                'container_id' => 'tester',
                'container' => false,
                'items_wrap' => '%3$s')), '<a>' );
        $menu = explode("\n", $menu_links);
        $menu2 = explode("\n", $menu_links);
        $break_point2 = floor(count($menu));
        $break_point = 5;?>

          <div id=nabvar_container class=text>
            <div id=mini> <?php
              echo $menu[0]; echo $menu[1]; ?>
            </div>

            <a href=# id=btn class=dropdown-btn> </a>
          </div>
      </ul>
    </nav>

    <ul id=content class=dropdown-content> <?php
      for($i=2; $i<$break_point2; $i++){
        if($i==2){ ?>
          <div id=first-half class=dropdown-content> <?php
        }
        echo $menu2[$i];
        if($i == ($break_point-1)) { ?>
          </div>
          <div id=second-half class=dropdown-content> <?php
        }
      } ?>
        </div>
    </ul>
  </header>

  <main role="main">
