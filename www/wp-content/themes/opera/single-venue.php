<?php

$map_search_parameters = sprintf("%s,%s/@%s,%s", urlencode(get_the_title()), "Portland,OR", $location['lat'], $location['lng']);

get_header();
?>

<!-- Begin Page -->
<?php get_template_part('partial-hero'); ?>

<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1><?php the_title(); ?></h1>

            <div class="intro"><?php the_content(); ?></div>
        </div>
    </div>

    <div class="medium-8 medium-centered columns">
        <div class="row left-align">
            <div class="medium-4 columns">
                <h4>Address</h4>
                <?php the_field('address'); ?>
            </div>
            <?php if (get_field('phone')): ?>
            <div class="medium-4 columns">
                <h4>Phone</h4>
                <a href="<?php echo format_phone(get_field('phone')); ?>" class="phone"><?php the_field('phone'); ?></a>
            </div>
            <?php endif; ?>
            <?php if (get_field('pdf')): ?>
            <div class="medium-4 columns">
                <h4>Seating Chart</h4>
                <a target="_blank" href="<?php echo get_field('pdf')['url']; ?>">Download PDF &raquo;</a>
            </div>
            <?php endif; ?>
        </div>

    </div>

</section>


<section id="getting-here" class="clearfix section getting-here custom-grid-section light">
    <section class="one-half left">
        <div class="inner-wrapper">

            <?php

            $location = get_field('google_map');

            if (!empty($location)):
            ?>
            <div class="venue-map">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            </div>
            <?php endif; ?>

        </div>
    </section>
    <section class="one-half right">
        <div class="inner-wrapper">
            <h2>Getting Here</h2>

            <?php the_field('driving_directions'); ?>


            <h3>Directions</h3>

            <?php the_field('location_details'); ?>
            <a target="_blank" href="https://www.google.com/maps/search/<?php echo $map_search_parameters ?>" class="btn-cta">Google Maps &raquo;</a>
        </div>
    </section>
</section>

<section id="accessibility" class="section clearfix accessibility">
    <div class="row">
        <div class="medium-12 medium-centered columns">
            <div class="row">
                <div class="medium-6 columns">
                    <h2>Accessibility</h2>
                        <?php the_field('accessibility'); ?>
                </div>
                <div class="medium-6 columns">
                    <h2>Food &amp; Beverages</h2>
                        <?php the_field('food_&_beverages'); ?>
                        <?php if (get_field('partners_link')): ?>
                        <a href="<?php maybe_add_http(get_field('partners_link')); ?>">Restaurant &amp; Hotel Partners &raquo;</a>
                        <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</section>



<!-- End Page -->

<?php get_footer(); ?>
