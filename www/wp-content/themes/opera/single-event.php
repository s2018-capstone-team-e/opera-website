<?php

get_header(); ?>

<!-- Begin Page -->

<section id="breadcrumbs" class="has-breadcrumb section has-rule">
    <div class="row">
        <div class="medium-12 columns">
            <ul class="breadcrumbs">
                <li><a href="/calendar">Calendar</a> &raquo;</li>
                <li><?php the_title(); ?></li>
            </ul>
        </div>
    </div>
</section>



<section class="clearfix custom-grid-section light">
    <section class="one-third left">
        <div class="inner-wrapper">
            <aside class="sidebar">
                <div class="show-for-small-only">
                    <h1 class="title"><?php the_title(); ?></h1>

                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                </div>

                <?php if (has_post_thumbnail()): ?>
                <div class="featured-image clearfix">
                    <?php the_post_thumbnail('gallery-preview'); ?>
                </div>
                <?php endif; ?>

                <?php if (get_field('location_name') && get_field('location_map')): ?>
                <?php $location = get_field('location_map'); ?>
                <div class="clearfix venue">
                    <h4 class="has-rule">Venue</h4>
                    <a href='http://maps.google.com/?q=<?php echo $location['lat'] . ', '. $location['lng']; ?>' class='venue'><?php the_field('location_name'); ?></a>
                </div>
                <?php endif; ?>

                <?php if (hasProductionTimes(get_the_ID())): ?>
                <div class="date-time clearfix">
                    <h4 class="has-rule">Performances</h4>
                    <?php if (have_rows("production_times")): ?>
                    <?php while (have_rows("production_times")):the_row(); ?>
                    <?php if (get_sub_field("date_&_time") != ""): ?>
                    <p class="performance-dates"><strong><?php echo date("F j, Y", strtotime(get_sub_field("date_&_time"))); ?></strong><br>
                    <?php echo date("l g:ia", strtotime(get_sub_field("date_&_time"))); ?>
                    </p>
                    <?php endif; ?>
                    <?php endwhile;?>
                    <?php endif; ?>
                </div>
                <?php endif; ?>

                <?php if (get_field("tickets_url")): ?>
                <div class="clearfix ">
                    <a href="<?php the_field("tickets_url"); ?>" class="tickets">Tickets &raquo;</a>
                </div>
                <?php endif; ?>

                <?php if (get_field("attendee_details")): ?>
                <div class="event-details clearfix">
                    <h4 class="has-rule">Details</h4>
                    <p><?php the_field('attendee_details'); ?></p>
                </div>
                <?php endif; ?>

                <?php get_template_part("partial-social-media"); ?>

            </aside>
        </div>
    </section>
    <section class="two-thirds right hide-for-small-only">
        <div class="inner-wrapper">

            <h1 class="title"><?php the_title(); ?></h1>

            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        </div>
    </section>
</div>




































        <!-- End Page -->

<?php get_footer(); ?>
