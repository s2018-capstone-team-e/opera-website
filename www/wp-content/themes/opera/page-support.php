<?php

get_header(); ?>

<!-- Begin Page -->

<?php get_template_part('partial-hero'); ?>


<section id="introduction" class="introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1><?php the_title(); ?> </h1>
            <p class="intro"><?php the_field("intro"); ?></p>
            <a href="<?php echo post_permalink(ID_by_slug('benefits-giving')); ?>"  class="button">Benefits of Giving</a>
        </div>
    </div>

</section>



<p> <?php the_field("support"); ?> </p>


<?php get_template_part('partial-expandable-section', 'support'); ?>




<!-- End Page -->

<?php get_footer(); ?>
