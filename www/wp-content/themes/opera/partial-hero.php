<?php

$hero_image = get_field("hero") ?: get_field('hero_image');

if ($hero_image): ?>
<!-- A Hero Appears -->

<section id="hero" class="has-image">

    <img src="<?php echo $hero_image['sizes']['hero']; ?>" alt="<?php echo $hero_image['alt']; ?>">

</section>
<?php endif; ?>