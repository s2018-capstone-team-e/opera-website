<?php

$time = current_time('timestamp');

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array(
    'post_type' => array('production', 'event'),
    'posts_per_page' => -1,
    /*'meta_key' => 'production_times_%_date_&_time',*/
    'orderby' => 'date',
    'order' => 'ASC',
    /*'meta_query' => array(
        array(
            'key' => 'production_times_%_date_&_time',
            'value' => $time,
            'compare' => '>=',
        )
    ),*/
    'paged' => get_query_var('paged')
);

$type = get_query_var('type');
$value = get_query_var('value');

if ($type && $value) {
    if ($type == 'production-type') {
        $args['production-type'] = $value;
    } elseif ($type == 'category') {
        $args['event-category'] = $value;
    }
}

$production_categories = get_terms('production-type', array(
    'orderby'    => 'count',
    'hide_empty' => 0
));


$event_categories = get_terms('event-category', array(
    'orderby'    => 'count',
    'hide_empty' => 0
));


$events = new WP_Query($args);

get_header(); ?>

<!-- Begin Page -->
<?php

global $post;
$post = get_post(ID_by_slug('calendar'));

setup_postdata($post);

 ?>
<section id="introduction" class="introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1><?php the_title(); ?></h1>
            <p class="intro"><?php the_field("intro"); ?></p>
        </div>
    </div>
</section>

<section id="highlights" class="section highlights has-rule">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Highlights</h2>

            <?php $highlights = get_field('event_widget'); ?>

            <?php if (is_array($highlights)): ?>
            <!-- Max-five Highlights -->
            <ul class="highlight-list medium-block-grid-5 small-block-grid-2">

                <?php foreach ($highlights as $key => $highlight):  ?>
                <?php if ($key > 5) {
     break;
 } // max 5 highlights?>

                <li class="highlight-obj">

                    <a href="<?php echo post_permalink($highlight->ID); ?>" class="thumbnail">
                        <?php if ($image = get_field('detail_image', $highlight->ID)): ?>
                        <img src="<?php echo $image['sizes']['season']; ?>">
                            <?php else: echo get_the_post_thumbnail($highlight->ID, 'season'); ?>
                        <?php endif; ?>
                    </a>
                    <h5><a href="<?php echo post_permalink($highlight->ID); ?>"><?php echo get_the_title($highlight->ID); ?></a></h5>
                    <span><?php echo !calendar_event_get_next_timestamp($highlight->ID) ? "No upcoming dates" : date("M Y", calendar_event_get_next_timestamp($highlight->ID)); ?></span>
                </li>

                <?php endforeach; ?>
                </ul>

            <?php endif; ?>

        </div>
    </div>
</section>


<!-- Begin Calendar -->

<section id="calendar" class="calendar custom-grid-section section clearfix light">
    <section class="one-third left">
        <div class="inner-wrapper">
            <div class="filters">
                <p class="help-text">Select a category to view a customized calendar</p>
                <ul class="filter-list">
                    <li class="cat-all <?php echo !$value ? 'active' : ''; ?>"><a href="<?php echo post_permalink(ID_by_slug('calendar')); ?>#calendar">All &raquo;</a></li>

                    <?php foreach (array_merge($production_categories, $event_categories) as $term): ?>
                    <li class="cat-item <?php echo $value == $term->slug ? 'active' : ''; ?>"><a href="<?php echo get_term_link($term); ?>#calendar"><?php echo $term->name; ?> &raquo;</a></li>
                    <?php endforeach ?>

                </ul>
            </div>
        </div>
    </section>

    <section class="two-thirds right">
        <div class="inner-wrapper">
                    <div class="interface">

            <?php $previousMonth = ''; $previousYear = ''; ?>

            <?php
                if ($events->have_posts()) {
                    $times = array();

                    while ($events->have_posts()) {
                        $events->the_post();
                        $ID = get_the_ID();

                        if (have_rows('production_times')) {
                            while (have_rows('production_times')) {
                                the_row();

                                if (get_sub_field('date_&_time') != '') {
                                    if (!is_array($times[strtotime(get_sub_field('date_&_time'))])) {
                                        $times[strtotime(get_sub_field('date_&_time'))] = array();
                                        array_push($times[strtotime(get_sub_field('date_&_time'))], $ID);
                                    } else {
                                        array_push($times[strtotime(get_sub_field('date_&_time'))], $ID);
                                    }
                                }
                            }
                        }
                    }
                    ksort($times);
                }

            ?>

            <!-- Begin Events by Month -->
            <?php if ($events->have_posts()): $i = 0; ?> <!-- Events Have Posts -->
            <?php foreach ($times as $key => $time): ?>
                <?php foreach ($time as $ID): $i++; ?>
                <?php if ($i == 11): ?>
                    <a href="#" class="button show-more-button">Show More</a>
                <?php endif; ?>
            <?php

                // set post to current production.
                $post = get_post($ID);
                setup_postdata($post);

                $postType = get_post_type(get_the_ID());

                $currentMonth = date("F", $key);
                $currentYear = date("Y", $key);
                $preShow = "";
                $postShow = "";

                $productionTime = date("l, F j, Y g:i a", $key);

                if ($preShowDate = calendar_event_get_pre_post(get_the_ID(), "pre-show", $key)) {
                    $preShow = date("l, F j, Y g:i a", $preShowDate);
                }
                if ($postShowDate = calendar_event_get_pre_post(get_the_ID(), "post-show", $key)) {
                    $postShow = date("l, F j, Y g:i a", $postShowDate);
                }

                if ($postType == "production") {
                    $relatedVenue = get_posts(array(
                        'connected_type' => 'related_venue_production',
                        'connected_items'=> get_the_ID(),
                        'nopaging' => true,
                    ));
                }
            if ($previousYear != $currentYear): ?>

            <!-- Year Navigation -->
<!--             <ul class="year-nav">  NOT SURE IF THIS STAYS ATM -at
                <li><a href=""><?php echo $currentYear; ?></a></li>
            </ul> -->

            <h3 class="year-label <?php echo($i > 10 ? "hidden": ""); ?>"><?php echo $currentYear; ?></h3>

            <?php endif;
            $previousYear = $currentYear;

            if ($previousMonth != $currentMonth) :?>

            <?php if ($previousMonth != ''): ?>
            </div>
            <?php endif; ?>

            <div class="month-wrap <?php echo $currentMonth; ?>">
            <h4 class="month-label has-rule <?php echo($i > 10 ? "hidden": ""); ?>"><?php echo $currentMonth; ?></h4>

            <?php endif;
            $previousMonth = $currentMonth;
            ?>

                <?php if ($postType == "production"): $type = wp_get_post_terms(get_the_ID(), 'production-type'); ?>
     <!-- line 220: post type = production -->

                <article class="event-obj event-type has-talks clearfix <?php echo($i > 10 ? "hidden": ""); ?>">
                    <a href="<?php the_permalink(); ?>" class="thumbnail">
                        <?php if ($type[0]->name == "Opera"): ?>
                            <img src="<?php $image = get_field("detail_image"); echo $image['sizes']['production-medium'];?>" alt="" width=150 height=150>
                        <?php else: ?>
                            <?php echo get_the_post_thumbnail(get_the_ID(), array(150, 150)); ?>
                        <?php endif; ?>
                    </a>
                    <div class="details">
                        <div class="event-title">
                            <span class="label"><?php  if (!empty($type)) {
                echo $type[0]->name;
            } ?></span>
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        </div>

                        <ul class="event-meta">
                            <li class="date"><?php echo $productionTime; ?></li>
                            <?php foreach ($relatedVenue as $venue): ?>
                            <li class="venue"><a href="<?php echo get_permalink($venue->ID); ?>"><?php echo $venue->post_title; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </article>

                <?php if ($preShow || $postShow): ?>
                <div class="pre-and-post-shows clearfix <?php echo($i > 10 ? "hidden": ""); ?>">
                    <?php if ($preShow): ?>
                    <div class="pre-show"><h5>Pre-show Talk</h5><span class="date"><?php echo $preShow; ?></span></div>
                    <?php endif; ?>
                    <?php if ($postShow): ?>
                    <div class="post-show"><h5>Post-show Talk</h5><span class="date"><?php echo $postShow; ?></span></div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>



                <?php elseif ($postType == "event"): ?>


                <article class="event-obj clearfix event-type <?php echo($i > 10 ? "hidden": ""); ?>">
                    <a href="<?php the_permalink(); ?>" class="thumbnail"><?php echo get_the_post_thumbnail(get_the_ID(), array(150, 150)); ?></a>
                    <div class="details">
                        <div class="event-title">
                            <span class="label"><?php the_terms(get_the_ID(), 'event-category'); ?></span>
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        </div>
                    </div>

                    <ul class="event-meta">
                        <li class="date"><?php echo $productionTime; ?></li>
                        <?php if (get_field('location_name')): ?>
                        <li class="venue no-link"><?php the_field('location_name') ?></li>
                        <?php endif; ?>
                    </ul>
                </article>
                <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; wp_reset_postdata(); ?>

            <?php else: ?>

            <p>There are no upcoming events that match these filters. </p>

            <?php endif; ?>

            </div>
            </div>
        </section>
    </section>

<!-- End Page -->

<?php get_footer(); ?>
