<?php global $post; ?>
<div class="share-event clearfix">
    <h4 class="has-rule">Share This</h4>
    <ul class="social-links">
        <li><a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink($post->ID); ?>" class="fb" target="_blank">Facebook</a></li>
        <li><a href="http://twitter.com/intent/tweet?text=<?php echo get_the_title($post->ID); ?>&url=<?php echo get_permalink($post->ID); ?>&via=portlandopera" class="tw" target="_blank">Twitter</a></li>
        <li><a href="https://plus.google.com/share?url=<?php echo get_permalink($post->ID); ?>" class="gl" target="_blank">Google+</a></li>
    </ul>
</div>
