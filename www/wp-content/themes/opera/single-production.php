<?php

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();
$production_type = get_term(get_field('production_type'), 'production-type')->slug;
$spn_image = get_field('broadway_sponsor', 'option');

?>

<!-- Begin Page -->
<?php get_template_part('partial-hero'); ?>

<section class="clearfix custom-grid-section light">
<section class="one-third left sidebar">
    <div class="inner-wrapper">

        <div class="show-for-small-only production-copy">
            <h1 class="title"><?php the_title(); ?></h1>
        <?php if (get_field("tickets_url")): ?>
        <div class="clearfix">
            <a href="<?php the_field("tickets_url"); ?>" class="tickets">Get Tickets &raquo;</a>
            <?php include(locate_template('template-parts/content-subscription-link.php')); ?>

        </div>
       <?php endif; ?>
       <div class="excerpt"><?php the_field("excerpt"); ?></div>

      </div>

        <?php if ($spn_image && "broadway" == $production_type): ?>
        <div class="broadway-sponsor">
            <img src="<?php echo $spn_image['sizes']['opera-small']; ?>">
        </div>
        <?php endif; ?>

        <div class="featured-image clearfix">

            <?php if ($gal_image = get_field("sidebar_square_image")): ?>
            <img src="<?php echo $gal_image['sizes']['gallery-preview']; ?>">
            <?php endif; ?>

            <div class="launch-gallery">
                <a href="#" data-reveal-id="photo-gallery" class="btn-photos">Photos</a>
            </div>
        </div>


        <div class="show-for-small-only production-copy">

        <?php the_field("content"); ?>

        </div>

        <div class="sb-venue clearfix">
        <?php
            $connected = get_posts(array(
                'connected_type' => 'related_venue_production',
                'connected_items' => get_the_ID(),
                'nopaging' => true,
            ));
        ?>
        <?php if (!empty($connected)): ?>
        <h4 class="has-rule">Venue</h4>
        <?php foreach ($connected as $venue): ?>
            <a href="<?php echo get_permalink($venue->ID); ?>" class="venue"><?php echo get_the_title($venue->ID); ?></a>
        <?php endforeach; ?>
        <?php endif; ?>

        </div>

        <?php if (hasProductionTimes(get_the_ID())): ?>
        <div class="sb-performances clearfix">
            <h4 class="has-rule">Performances</h4>
            <?php if (have_rows("production_times")): ?>
            <?php while (have_rows("production_times")):the_row(); ?>
            <p class="performance-dates"><strong><?php echo date("F j, Y", strtotime(get_sub_field("date_&_time"))); ?></strong><br>
            <?php echo date("l g:ia", strtotime(get_sub_field("date_&_time"))); ?>
            </p>
            <?php endwhile;?>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <?php if (get_field("tickets_url")): ?>
            <div class="clearfix">
                <a href="<?php the_field("tickets_url"); ?>" class="tickets">Get Tickets &raquo;</a>
             <?php include(locate_template('template-parts/content-subscription-link.php')); ?>
            </div>
        <?php endif; ?>

        <?php if (have_rows("production_sponsor")): ?>
        <?php while (have_rows("production_sponsor")): the_row(); ?>
        <div class="sb-production-sponsor clearfix">
            <h4 class="has-rule">Sponsor</h4>
            <?php $sponsorLogo = get_sub_field("logo"); ?>
            <a href="<?php the_sub_field("sponsor_url"); ?>" target="_blank"><img src="<?php echo $sponsorLogo["sizes"]["production-sponsor"]; ?>" alt=""></a>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>

            <?php
                $connected = get_posts(array(
                    'connected_type' => 'related_production',
                    'connected_items' => get_the_ID(),
                    'nopaging' => true,
                ));
            ?>

      <?php if (count($connected) > 0): ?>
        <div class="related-productions clearfix">
          <h4>You Might Also Like</h4>
            <ul>

            <?php foreach ($connected as $production): ?>
                <?php $type = wp_get_post_terms($production->ID, 'production-type'); ?>
                <li><a href="<?php echo get_permalink($production->ID); ?>">

                        <?php if ($type[0]->slug == 'opera'): ?>
                        <img src="<?php $image = get_field('detail_image', $production->ID); echo $image['sizes']['production-connection']; ?>">
                        <?php elseif ($type[0]->slug == 'broadway'): ?>
                        <img src="<?php $image = get_field('sidebar_square_image', $production->ID); echo $image['sizes']['production-connection']; ?>">
                        <?php endif; ?>

                        <h5><?php echo get_the_title($production->ID); ?></h5>
                        <?php
                            $relatedProdVenue = get_posts(array(
                                'connected_type' => 'related_venue_production',
                                'connected_items' => $production->ID,
                                'nopaging' => true,
                            ));
                        ?>
                        <?php foreach ($relatedProdVenue as $venue): ?>
                        <span class="venue"><?php echo get_the_title($venue->ID); ?></span>
                        <?php endforeach; ?>
                        <!-- <?php
                            if (have_rows('production_times', $production->ID)): ?>
                        <?php while (have_rows('production_times', $production->ID)): the_row(); ?>
                        <span><?php echo date("n/y", strtotime(get_sub_field("date_&_time"))); ?></span>
                        <?php break; endwhile; ?>
                        <?php endif; ?>  -->
                    </a></li>
            <?php endforeach; ?>
            </ul>
        </div>
      <?php endif; ?>
      <?php if (get_term(get_field('production_type')[0], 'production-type')->slug == 'broadway' && (get_field('production_facebook_url') || get_field('production_twitter_url') || get_field('production_instagram_url'))): ?>

        <div class="broadway-social-links">
            <?php if ($url = get_field('production_facebook_url')): ?>
            <a href="<?php echo maybe_add_http($url); ?>">Facebook</a>
            <?php endif; ?>
            <?php if ($url = get_field('production_twitter_url')): ?>
            <a href="<?php echo maybe_add_http($url); ?>">Twitter</a>
            <?php endif; ?>
            <?php if ($url = get_field('production_instagram_url')): ?>
            <a href="<?php echo maybe_add_http($url); ?>">Instagram</a>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <?php get_template_part('partial-social-media'); ?>

    </div>
</section>


<section class="production-content two-thirds right">

    <div class="inner-wrapper production-copy">
        <div class="hide-for-small-only">
            <h1 class="title"><?php the_title(); ?></h1>
        <?php if (get_field("tickets_url")): ?>
          <div class="clearfix">
              <div class="tickets">
              <a title="Get tickets to this production" href="<?php the_field("tickets_url"); ?>"  >Get Tickets</a>
              <?php include(locate_template('template-parts/content-subscription-link.php')); ?>
              </div>
          </div>
        <?php endif; ?>

            <div class="excerpt"><?php the_field("excerpt"); ?></div>

        <?php the_field("content"); ?>

        </div>

        <div class="production-media">
            <?php if ($photos = get_field("gallery")): ?>
            <ul>
                <li class="photos"><a href="#"  data-reveal-id="photo-gallery">Photos</a></li>
            </ul>


          <!-- Modal Gallery content -->

            <ul id="production-gallery-grid">
            <?php $i = 0; foreach ($photos as $photo): $i++; ?>
                <li><?php //print_r($photo);?>
                    <img  data-reveal-id="photo-gallery" src="<?php echo $photo['sizes']['medium']; ?>" alt="slide <?php echo $i; ?>" />
                </li>
            <?php endforeach; ?>
            </ul>


          <?php endif; ?>
                <?php if (have_rows('video_links') || have_rows('audio_links')): ?>
            <dl class="" id="media" data-accordion>
                <?php if (have_rows('video_links')): ?>
                <dd class="">
                    <a href="#video" class="media-type  media-header">Videos</a>
                    <div id="video" class="content">

                <?php while (have_rows('video_links')): the_row(); ?>
          <h5><?php the_sub_field('title'); ?></h5>
          <?php $code = get_sub_field('embed_code'); echo apply_filters('the_content', $code); ?>

                           <!-- <li><a href="<?php the_sub_field(get_sub_field('file') ? 'file' : 'link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>-->
                <?php endwhile; ?>

                    </div>
                </dd>
                <?php endif; ?>

                <?php if (have_rows('audio_links')): ?>
                <dd class=" ">
                    <a href="#audio" class="media-type media-header">Audio</a>
                    <div id="audio" class="content">
                        <ul>
                <?php while (have_rows('audio_links')): the_row(); ?>
                            <!--<li><a href="<?php the_sub_field(get_sub_field('file') ? 'file' : 'link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li> -->
             <h5><?php the_sub_field('title'); ?></h5>
            <audio controls>
                  <source src="<?php the_sub_field(get_sub_field('file') ? 'file' : 'link'); ?>" type="audio/mpeg">
                  Your browser does not support the audio element.
            </audio>

                <?php endwhile; ?>
                        </ul>
                    </div>
                </dd>
                <?php endif; ?>
            </dl>
                <?php endif; ?>

        </div>

        <div class="production-extras">
            <dl class="accordion" id="extras" data-accordion>
              <?php if (get_field("performers")): ?>
              <dd class="accordion-navigation">
                <a href="#cast" class="extras-type active">Cast</a>
                <div id="cast" class="content active">
                  <?php the_field("performers"); ?>
                </div>
              </dd>
              <?php endif; ?>
              <?php if (get_field("plot_&_program_notes")): ?>
              <dd class="accordion-navigation">
                <a href="#notes" class="extras-type">Plot &amp; Program Notes</a>
                <div id="notes" class="content">
                  <p><?php the_field("plot_&_program_notes"); ?></p>
                </div>
              </dd>
              <?php endif; ?>
            </dl>
        </div>
    </div>


</section>
</section>

<section id="production-navigation" class="section nav-section">
    <div class="row">
        <div class="medium-12 columns">

            <ul class="production-nav">
                <?php
        $next = get_next_post();
        $next_type = wp_get_post_terms($next->ID, 'production-type')[0];
        $prev = get_previous_post();
        $prev_type = wp_get_post_terms($prev->ID, 'production-type')[0]; ?>
                <li class="prev"><?php previous_post_link('%link', '&laquo; '.$prev_type->name.': %title'); ?></li>

                <li class="next"><?php next_post_link('%link', $next_type->name.': %title &raquo;'); ?></li>
            </ul>


        </div>
    </div>
</section>

<?php if ($photos = get_field("gallery")): ?>

<!-- Modal Gallery content -->
<div id="photo-gallery" class="reveal-modal gallery medium" data-reveal>
    <ul class="gallery-slider" data-orbit data-options="variable_height:true;">
        <?php $i = 0; foreach ($photos as $photo): $i++; ?>
        <li>
            <img src="<?php echo $photo['url']; ?>" alt="slide <?php echo $i; ?>" />
            <div class="modal-caption"><?php echo $photo['caption']; ?></div>
        </li>
        <?php endforeach; ?>
    </ul>
  <a class="close-reveal-modal">&#215;</a>
</div>
<?php endif; ?>

<?php endwhile; endif; ?>


<?php
if (get_field('pixel_tracking_code')) {
            echo get_field('pixel_tracking_code', false, false);
        }
?>

        <!-- End Page -->

<?php get_footer(); ?>
