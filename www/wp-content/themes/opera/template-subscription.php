<?php

/*
     Template Name: Subscription
*/

get_header(); ?>

<!-- Begin Page -->
<?php get_template_part('partial-hero'); ?>

<?php get_template_part('partial-breadcrumbs'); ?>


<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-10 medium-centered columns">
            <h1><?php the_title(); ?> </h1>

            <p class="intro"><?php the_field("intro"); ?></p>
        </div>
    </div>
</section>

<div class="row">
    <div class="medium-8 large-12 columns production-grid subscription-opera-lineup">
      <?php $season = get_field('season'); ?>

      <h5><?php the_field('package_details');?></h5>
        <?php $post_objects = get_field('productions');

if ($post_objects): ?>
    <ul class="production-count medium-block-grid-3">

    <?php foreach ($post_objects as $post): // variable must be called $post (IMPORTANT)?>
        <?php setup_postdata($post); ?>
          <li class="production-obj is-opera">
      <div class="image-wrapper">
          <a href="<?php the_permalink(); ?>" class="thumbnail">

          <?php if ($image = get_field('detail_image')): ?>
            <img src="<?php echo $image['sizes']['season']; ?>">
          <?php endif; ?>
          </a>

          <div class="details">
              <a href="<?php the_permalink(); ?>" class="view-more">View More &raquo;</a>
          </div>
      </div>

      <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
  </li>
    <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly?>
<?php endif; ?>




    </div>

    </div>
</div>

<?php get_template_part("partial", $post->post_name); ?>

<div class="row">
    <div class="medium-12 columns entry-content">

        <?php the_content(); ?>

    </div>
</div>

<!-- End Page -->

<?php get_footer(); ?>