<?php

get_header(); ?>

<!-- Begin Page -->


<section id="top" class="section _grid">
    <div id="exper">
        <h1><?php the_title(); ?> </h1>
        <p class="intro"><?php the_field("intro"); ?></p>


    </div>

    <div id="right">
        <div id="partOne">
            <h3>Pre- &amp; Post-Show Events</h3>
            <p><?php the_field("pre_post_show_content"); ?></p>
        </div>
        <div id="partTwo">
            <h3>Pre-Show</h3>
            <p><?php the_field('pre_show'); ?></p>
        </div>
        <div id="partTwo">
            <h3>Post-Show</h3>
            <p><?php the_field('post_show'); ?></p>
        </div>
    </div>
</section>

<!--
//<section id="pre-post-events" class="section light-on-dark has-background introduction pre-post-events">
  //
</section>
-->

<section id="venues-partners" class="section">
        <div id="venue">
            <h3>Venues</h3>
            <p><?php the_field("venues_content"); ?></p>
            <a href="/venues" id="ven-button" class="button">Take a Look</a>
        </div>
        <div id="partner">
            <h3>Restaurant &amp; Hotel Partners</h3>
            <p><?php the_field("restaurant_&_hotel_partners_content"); ?>
        </div>

</section>

<section id="spon">
    <div class="row restaurant-partners">

            <h4>Restaurant Sponsors</h4>

            <?php if (have_rows('restaurant_sponsors')): ?>
        <div class="spon-list">
                <?php while (have_rows('restaurant_sponsors')): the_row(); ?>
                    <!-- <h5><a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('name'); ?></a></h5> -->
                    <div class="Spon-thumb">
                        <a href="<?php the_sub_field('url'); ?>" class="thumbnail"><img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt=""></a>
                    </div>
                <?php endwhile; ?>
        </div>
            <?php endif; ?>

    </div>

    <div class="row hotel-partners">
            <h4>Hotel Sponsors</h4>
            <?php if (have_rows('hotel_sponsors')): ?>
            <div class="spon-list">
                <?php while (have_rows('hotel_sponsors')): the_row(); ?>
                    <!-- <h5><a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('name'); ?></a></h5> -->
                    <div class="Spon-thumb">
                        <a href="<?php the_sub_field('url'); ?>" class="thumbnail"><img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt=""></a>
                    </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
    </div>
</section>
<!-- End Page -->

<?php get_footer(); ?>
