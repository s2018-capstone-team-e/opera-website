<?php

get_header(); ?>

<!-- Begin Page -->
<?php get_template_part('partial-hero'); ?>

<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
           <h1><?php the_title(); ?></h1>
            <p class="intro"><?php the_field("intro"); ?></p>
      <!--  <a href="<?php the_field("hero_link"); ?>" class="btn-cta">Ticket Policies &raquo;</a> -->
        </div>
    </div>

</section>



<div class="row purchase-methods">
    <div class="medium-10 medium-centered columns">
        <div class="row">
            <div class="medium-4 columns">
                <h3>Online</h3>
                <ul> <!-- Ticketmaster links -->
                    <?php if (get_field('show_subscription_link', 'option')): ?>
                    <li><a href="<?php the_field("subscribe_link", "option"); ?>">Subscribe or Renew &raquo;</a></li>
                    <?php endif; ?>
                    <li><a href="<?php the_field("buy_individual_link"); ?>">Buy individual show tickets &raquo;</a></li>
                    <li><a href="<?php the_field("manage_account_link"); ?>">Manage account &raquo;</a></li>
                </ul>
            </div>
            <div style="border-right-style: solid; border-left-style: solid; border-width: 1px;" class="medium-4 columns">
                <h3>In Person</h3>
                <address>
                    <a href="/venue/the-hampton-opera-center/"><strong><?php the_field("in_person_title"); ?></strong></a> <br>
                    <?php the_field("in_person_address"); ?>
                </address>
                <p><?php the_field("in_person_hours"); ?></p>

            </div>
            <div class="medium-4 columns">
                <h3>By Phone</h3>
                <a href="tel:<?php the_field("by_phone_1"); ?>" class="phone"><?php the_field("by_phone_1"); ?></a>
                <a href="tel:<?php the_field("by_phone_2"); ?>" class="phone"><?php the_field("by_phone_2"); ?></a>
                <p><?php the_field("by_phone_hours"); ?></p>
            </div>
        </div>

    </div>
    <div style="text-align: center; margin-top: 50px">
         <a href="<?php the_field("hero_link"); ?>" class="button">Ticket Policies</a>
    </div>


</div>


<section class="ticket-info-section">
    <div class="ticket-info-header">
        <div class="ticket-info-label">
            <h1>SUBSCRIBER BENEFITS</h1>
        </div>

        <p><?php the_field("subscriber_benefits_excerpt"); ?></p>
    </div>


    <div class="ticket-info-content">
        <div class="row">
            <div class="medium-12 columns">
                <div style="text-align: left; font-family: Georgia"; class="two-columns">

                    <?php the_field("subscriber_benefits_content"); ?>
                </div>
            </div>

        </div>
    </div>

</section>



</section>


<section id="Rush tickets">
    <div class="row">
        <div class="ticket-info-header">
            <div class="ticket-info-label">
                 <h1>RUSH TICKETS</h1>
            </div>
            <p><?php the_field("rush_tickets_excerpt"); ?></p>
        </div>
        <div class="ticket-info-content">
            <p><?php the_field("rush_tickets_content"); ?></p>

            <div class="medium-4 columns">

                <?php if (have_rows("sponsors")): ?>
                    <h4>Sponsors</h4>
                    <ul class="sponsor-list"> <!-- Logos Link to sponsor websites -->
                        <?php while (have_rows("sponsors")): the_row(); ?>
                            <li><a href="<?php the_sub_field("url"); ?>"><img src="<?php $image = get_sub_field("image"); echo $image['url']; ?>" alt=""></a></li>
                        <?php endwhile; ?>

                    </ul>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>


<section class="ticket-info-section-group-sales">
    <div class="ticket-info-header">
        <div class="ticket-info-label">
            <h1>GROUP SALES</h1>
        </div>

        <p><?php the_field("group_sales_excerpt"); ?></p>
    </div>


    <div class="ticket-info-content">
        <div class="row">
            <?php the_field("group_sales_content"); ?>
        </div>

    </div>

</section>

<section id="ticket-info-section-arts">
    <div class="ticket-info-header">
        <div class="ticket-info-label">
            <h1>ARTS FOR ALL</h1>
        </div>

        <p><?php the_field("arts_excerpt"); ?></p>
    </div>


        <div class="ticket-info-content">
            <p> <?php the_field("arts_content"); ?> </p>
        </div>


</section>





<section id="venues">
    <div class="ticket-info-header">
        <div class="ticket-info-label">
            <h1>Venues</h1>
        </div>
        <div class="ticket-info-content">
            <p><?php the_field("venues_content"); ?></p>
            <div style="text-align: center">
                 <a href="/venues" class="button">Take a Look</a>
            </div>
        </div>

    </div>
</section>





        <!-- End Page -->

<?php get_footer(); ?>
