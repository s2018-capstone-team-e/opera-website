// In case we forget to take out console statements. IE becomes very unhappy when we forget. Let's not make IE unhappy
if (typeof console === 'undefined') {
  var console = {};
  console.log = console.error = console.info = console.warn = console.trace = console.dir = console.dirxml = function() {};
}
if (typeof console.group === 'undefined') {
  console.group = console.groupEnd = console.time = console.debug = console.timeEnd = console.assert = console.profile = console.info = function() {};
}

jQuery(function($) {
  // on projects, move excerpt to after title
  $('.post-type-post #postexcerpt, .post-type-page #postexcerpt').insertAfter(
    '#titlediv'
  );

  $('#acf_acf_production-type').insertAfter('#submitdiv');
});
