jQuery(function($) {
  /* Primary nav menu functionality */

  var menu = $('.top-bar');
  var menuButton = $('.dropdown-btn');
  //var menuParent = $('.top-bar');

  var menuParent = $('.dropdown-content');
  menuButton.on('click', function(e) {
    e.preventDefault();

    menu.toggleClass('active');

    var $this = $(this);

    if (menuParent.hasClass('active')) {
      menuParent.removeClass('active');
     } else {
      menuParent.addClass('active');
    }
  });

  $(document).mouseup(function(e) {
    if (
      !menuButton.is(e.target) &&
      !menu.is(e.target) && // if the target of the click isn't the container...
      menu.has(e.target).length === 0
    ) {
      // ... nor a descendant of the container
      menuParent.removeClass('active');
    }
  });

  /* Expanding content section functionality */
  $('.expandable-content .panel').on('click', function(e) {
    // do nothing if click came via the list of links
    if (e.target.tagName == 'A') {
      return;
    }

    var $this = $(this);

    $this
      .parent()
      .find('.inner-content')
      .animate(
        {
          height: 'toggle',
        },
        300,
        function() {
          $this.closest('.expandable-content').toggleClass('open');
        }
      );

  });
//////////
  /* Open panel if it's been specified in the URL */
  if (window.location.hash) {
    $(window.location.hash + ' .panel').click();
  }

  /* Fix for Foundation bug when Orbit slider used within Reveal modal */
  $('.reveal-modal').on('opened', function() {
    $(window).trigger('resize');
  });

  /* Foundation Accordion customization */
  $('#extras').on('toggled', function() {
  });

  $('#extras .extras-type').on('click', function() {
    $(this).toggleClass('active');
    $('.extras-type')
      .not($(this))
      .removeClass('active');
  });

  $('#media .media-type').on('click', function() {
    $(this).toggleClass('active');
    $('.media-type')
      .not($(this))
      .removeClass('active');
  });

  /* Calendar `show more` functionality */
  $('.show-more-button').on('click', function(e) {
    e.preventDefault();
    var i = 0,
      next = $(this).next()[0];

    for (i; i < 10; i++) {
      // show whatever we've come across
      $(next).removeClass('hidden');

      if (typeof $(next).next()[0] !== 'undefined') {
        // decrement i since we're not showing an article
        if (next.nodeName !== 'ARTICLE') {
          i--;
        }

        next = $(next).next()[0];
      } else {
        // if there is no next element lets see if there's more items to show
        if (typeof next !== 'undefined') {
          next = $(next)
            .parent()
            .next()
            .children()[0];
        } else if ($('article').hasClass('hidden')) {
          next = $(this)
            .parent()
            .next()
            .children()[0];
        } else {
          $(this).addClass('hidden');
        }
      }

      // append next where we left off
      if (i === 8) {
        $(this).insertAfter(next);
      }
    }
  });

  /* Count Season Line-up, update grid accordingly */
  /*var seasonLineup;

    let show_lineup = function() {
        $('.production-count').fadeIn(600);
    };

    let load_lineup = function() {
        seasonLineup = $('.production-count li').length;
        if (seasonLineup > 3) {
            $('.production-count').removeClass('medium-block-grid-3');
            $('.production-count').addClass('medium-block-grid-' + seasonLineup);
        }
        setTimeout(function(){show_lineup()}, 300);

    };

    load_lineup();*/

  // Analytics Tracking
  if (typeof ga == 'function') {
    var $outbound = $('a').filter(function() {
      return this.hostname && this.hostname !== location.hostname;
    });

    // outbound

    $(document).on('click', $outbound, function(e) {
      ga('send', {
        hitType: 'event', // Required.
        eventCategory: 'Outbound links', // Required.
        eventAction: 'click', // Required.
        location: e.currentTarget.href,
      });
    });

    // pdf downloads

    $(document).on('click', "a[href$='pdf']", function(e) {
      ga('send', {
        hitType: 'event', // Required.
        eventCategory: 'Download links', // Required.
        eventAction: 'click', // Required.
        location: e.currentTarget.href,
      });
    });
  }
});

/*
*  render_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type    function
*  @date    8/11/2013
*  @since   4.3.0
*
*  @param   $el (jQuery element)
*  @return  n/a
*/

function render_map($el) {
  // var
  var $markers = $el.find('.marker');

  // vars
  var args = {
    zoom: 16,
    center: new google.maps.LatLng(0, 0),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  };

  // create map
  var map = new google.maps.Map($el[0], args);

  // add a markers reference
  map.markers = [];

  // add markers
  $markers.each(function() {
    add_marker($(this), map);
  });

  // center map
  center_map(map);
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type    function
*  @date    8/11/2013
*  @since   4.3.0
*
*  @param   $marker (jQuery element)
*  @param   map (Google Map object)
*  @return  n/a
*/

function add_marker($marker, map) {
  // var
  var latlng = new google.maps.LatLng(
    $marker.attr('data-lat'),
    $marker.attr('data-lng')
  );

  // create marker
  var marker = new google.maps.Marker({
    position: latlng,
    map: map,
  });

  // add to array
  map.markers.push(marker);

  // if marker contains HTML, add it to an infoWindow
  if ($marker.html()) {
    // create info window
    var infowindow = new google.maps.InfoWindow({
      content: $marker.html(),
    });

    // show info window when marker is clicked
    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map, marker);
    });
  }
}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type    function
*  @date    8/11/2013
*  @since   4.3.0
*
*  @param   map (Google Map object)
*  @return  n/a
*/

function center_map(map) {
  // vars
  var bounds = new google.maps.LatLngBounds();

  // loop through all markers and create bounds
  $.each(map.markers, function(i, marker) {
    var latlng = new google.maps.LatLng(
      marker.position.lat(),
      marker.position.lng()
    );

    bounds.extend(latlng);
  });

  // only 1 marker?
  if (map.markers.length == 1) {
    // set center of map
    map.setCenter(bounds.getCenter());
    map.setZoom(16);
  } else {
    // fit to bounds
    map.fitBounds(bounds);
  }
}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type    function
*  @date    8/11/2013
*  @since   5.0.0
*
*  @param   n/a
*  @return  n/a
*/

(function($){
    $(document).ready(function() {
        $('.venue-map').each(function() {
          render_map($(this));
        });
    });
})(jQuery);
