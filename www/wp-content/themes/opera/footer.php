        </main>

        <!-- Begin Footer -->
        <div class="footer-color-cap"></div>
        <footer role="contentinfo">
            <div class="row">
                <div class="medium-4 columns follow-po">
                    <div class="group">
                        <h4 class="has-rule">Follow Us</h4>
                        <ul class="social-links">
                            <li><a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="fb">Facebook</a></li>
                            <li><a href="<?php the_field('twitter', 'option'); ?>" target="_blank" class="tw">Twitter</a></li>
                            <li><a href="<?php the_field('youtube', 'option'); ?>" target="_blank" class="yt">Youtube</a></li>
                        </ul>
                    </div>
                    <div class="group">
                        <h4 class="has-rule">Newsletter</h4>
                        <a href="https://oss.ticketmaster.com/html/home.htmI?team=portlandopera" target="_blank" class="subscribe">Subscribe to Opera Buzz <span class="icon"></span></a>
                    </div>

                </div>
                <div class="medium-3 medium-offset-1 columns find-po">
                    <h4 class="has-rule">Find Us</h4>
                    <address>
                        <strong>The Hampton Opera Center</strong> <br>
                        <a href="https://www.google.com/maps/preview?ie=UTF-8&fb=1&gl=us&sll=45.5061491,-122.6639903&sspn=0.023099,0.0439462&q=211+SE+Caruthers+St,+Portland,+OR+97214&ei=flQGVJyVNOj5iwLYkoHICw&ved=0CB8Q8gEwAA" target="_blank"><?php the_field('address', 'option'); ?><span class="icon"></span></a>
                    </address>
                    <?php if (have_rows('phone_numbers', 'option')): ?>
                    <?php while (have_rows('phone_numbers', 'option')): the_row(); ?>
                    <span class="phone"><?php the_sub_field('display_phone_number'); ?></span> <br>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="medium-3 medium-offset-1 columns po-hours">
                    <h4 class="has-rule">Our Office Hours</h4>
                    <p><?php the_field('hours', 'option'); ?></p>
                </div>
            </div>

            <section class="footer-nav">
                <div class="row">
                    <div class="medium-8 columns">
                        <?php wp_nav_menu(array('menu' => 'Footer Menu', 'menu_class' => 'footer-menu', 'container' => false )); ?>
                    </div>
                    <div class="medium-4 columns">
                        <p class="legal">&copy; <?php echo date('Y'); ?> Portland Opera</p>
                    </div>
                </div>
            </section>
        </footer>
        <?php wp_footer(); ?>
        <script>
            jQuery(document).ready(function($) {
              $('.flexslider').flexslider({
                animation: "slide",
                pauseOnHover: true,
                controlNav: false,
                directionNav: false
              });
            });
        </script>
    </body>
</html>
