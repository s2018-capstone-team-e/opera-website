<?php

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- Begin Page -->

<section id="breadcrumbs" class="has-breadcrumb section has-rule">
    <div class="row">
        <div class="medium-12 columns">
            <ul class="breadcrumbs">
                <li><a href="<?php echo get_permalink(get_option('page_for_posts')); ?>">News</a> &raquo;</li>
                <li><?php the_title(); ?></li>
            </ul>
        </div>
    </div>
</section>

<div class="row">

    <div class="medium-8 medium-centered columns">
        <span class="date"><?php the_time('F j, Y') ?></span>
        <h1 class="post-title"><?php the_title(); ?></h1>

        <div class="entry-content">
            <?php the_content(); ?>
        </div>

    </div>

</div>

<?php endwhile; ?>
<?php endif; ?>

        <!-- End Page -->

<?php get_footer(); ?>
