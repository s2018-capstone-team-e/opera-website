<?php get_header(); ?>

    <!-- Begin Page -->
    <section id="introduction" class="page-intro introduction section">
        <div class="row">
            <div class="medium-10 medium-centered columns">
                <div id="intro-financial">
                    <h1>Financial Information</h1>
                    <p class="intro"><p>The Portland Opera adheres to the highest standards of accountability and transparency. As part of this commitment, the following financial information is made available.</p>
                </div>
            </div>
        </div>
    </section>

    <div id="financial-info">
        <div id="annual-report" class="financial">
            <h4>Annual Report:</h4>
            <ul>
                <li><span style="color: #000000;"><a style="color: #000000;" href="http://www.portlandopera.org/wp-content/uploads/2018/01/Portland-Opera_2017_-Annual-Report.pdf" target="_blank" rel="noopener"><em>2017 Annual Report</em></a></span></li>
            </ul>
        </div>

        <div id="audit-info" class="financial">
            <h4>Audit Information</h4>
            <ul>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/12/Audited-Financial-Statements-for-Sept-2017.pdf" target="_blank" rel="noopener"><em>Audited Financial Statements for the period ended September 2017<u></u></em><u></u></a><u></u></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/Audited-Financial-Statements-for-period-ended-June-2016.pdf" target="_blank" rel="noopener"><em>Audited Financial Statements for the period ended June 2016</a></em></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/Audited-Financial-Statements-for-period-ended-June-2015.pdf" target="_blank" rel="noopener"><em>Audited Financial Statements for the period ended June 2015<u></u></em></a><u></u></li>
            </ul>
        </div>

        <div id="tax-info" class="financial">
            <h4>Tax Information</h4>
            <ul>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2018/02/Form-990-for-Sept-2017-Opera.pdf">Form 990 for the period ended September 2017 (Opera)</a></em></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2018/02/Form-990-for-Sept-2017-Broadway.pdf">Form 990 for the period ended September 2017 (Broadway)</a></em></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/Form-990-for-period-ended-Sept-2016-Opera.pdf" target="_blank" rel="noopener"><em>Form 990 for the period ended September 2016 (Opera)</a></em></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/Form-990-for-period-ended-Sept-2016-Broadway.pdf" target="_blank" rel="noopener"><em>Form 990 for the period ended September 2016 (Broadway)</a></em></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/Form-1128-changing-fiscal-year-to-September-1.pdf" target="_blank" rel="noopener"><em>Form 1128 changing fiscal year from June to September</a></em></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/Form-990-for-period-ended-June-2016-Opera.pdf" target="_blank" rel="noopener"><em>Form 990 for the period ended June 2016 (Opera)</a></em></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/Form-990-for-period-ended-June-2016-Broadway.pdf" target="_blank" rel="noopener"><em>Form 990 for the period ended June 2016 (Broadway)</a></em></li>
            </ul>
        </div>

        <div id="tax-exempt" class="financial">
            <h4>Tax Exempt Letters</h4>
            <ul>
                <li><em><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/IRS-Tax-Exempt-Letter-Opera.pdf" target="_blank" rel="noopener">IRS Tax Exempt Letter (Opera)</a></em></li>
                <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/IRS-Tax-Exempt-Letter-Broadway.pdf" target="_blank" rel="noopener"><em>IRS Tax Exempt Letter (Broadway)<u></u></em></a><u></u></li>
            </ul>
        </div>

        <div id="donor-policy" class="financial">
            <h4>Donor Policy</h4>
          <ul>
               <li><a href="http://www.portlandopera.org/wp-content/uploads/2017/11/Donor-Privacy-Policy-Statement.pdf" target="_blank" rel="noopener"><u></u><em>Donor Privacy Policy Statement<u></u></em></a></li>
          </ul>
       </div>
    </div>

    <div id="add-info">
        <p>For questions or any additional financial information, please contact:</p>
        <p>Richard Seals, CPA CMA CFM CFE CGMA<br />
            Director of Finance &amp; Administration<br />
            Portland Opera/U.S. Bank Broadway in Portland<br />
            503-321-5269 | <a href="mailto:rseals@portlandopera.org"><strong><span style="color: #800080;">rseals@portlandopera.org</span></strong></a></p>
        <p>&nbsp;</p>
    </div>
    <!-- End Page -->

<?php get_footer(); ?>