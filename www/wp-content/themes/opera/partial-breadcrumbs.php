<!-- Breadcrumbs -->
<?php if (is_page() && has_parent()): ?>

<section id="breadcrumbs" class="has-breadcrumb section has-rule">
    <div class="row">
        <div class="medium-12 columns">
            <ul class="breadcrumbs">

                <?php foreach (get_ancestors(get_the_ID(), get_post_type()) as $parent_page): ?>
                    <li><a href="<?php echo post_permalink($parent_page); ?>"><?php echo get_the_title($parent_page); ?></a> &raquo;</li>
                <?php endforeach; ?>

                <li><?php the_title(); ?></li>
            </ul>
        </div>
    </div>
</section>

<?php endif; ?>