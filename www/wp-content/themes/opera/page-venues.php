<?php
$args = array('post_type'=>'venue');

$venues = new WP_Query($args);

get_header(); ?>

<!-- Begin Page -->
<?php get_template_part('partial-hero'); ?>

<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1><?php the_title(); ?> </h1>
            <p class="intro"><?php the_field("intro"); ?></p>
            <p><?php the_content(); ?></p>
        </div>
    </div>

</section>


<section id="venues" class="venues section">
    <div class="row">
        <div class="medium-12 columns">
            <ul class="medium-block-grid-3">
                <?php while (have_rows('venue_list')): the_row();

                // wordpress gymnastics to override post
                $post_object = get_sub_field('venue');
                // override $post
                $post = $post_object;
                setup_postdata($post);
                $map_search_parameters = sprintf("%s,%s/@%s,%s", urlencode(get_the_title()), "Portland,OR", $location['lat'], $location['lng']);

                ?>
                <li class="venue-obj">
                    <div class="image-wrapper">
                        <a href="<?php the_permalink(); ?>" class="thumbnail">
                            <?php the_post_thumbnail('production-medium'); ?>

                        </a>
                        <div class="details">
                            <a href="<?php the_permalink(); ?>" class="view-more">View More &raquo;</a>
                        </div>
                    </div>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <p><?php echo get_sub_field('venue_blurb') ?: the_excerpt(); ?></p>
                    <ul>
                        <li><a target="_blank" href="https://www.google.com/maps/search/<?php echo $map_search_parameters ?>" class="btn-cta">View Map &raquo;</a>
</li>
                        <?php if (get_field('pdf')): ?>
                        <li><a href="<?php echo get_field('pdf')['url']; ?>" target="_blank">Seating Chart &raquo;</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?php wp_reset_postdata(); ?>
                <?php endwhile; ?>

        </div>
    </div>
</section>




<section id="experience" class="section has-background experience light-on-dark">
    <div class="row section-intro">
        <div class="medium-8 medium-centered columns">
            <?php the_field('experience_content'); ?>
            <a href="<?php the_field("experience_link"); ?>" class="button">Experience the Opera</a>
        </div>
    </div>
</section>



        <!-- End Page -->

<?php get_footer(); ?>
