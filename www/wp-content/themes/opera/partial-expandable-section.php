<?php /* Expandable Content Sections */ ?>

<?php if (have_rows('expandable_section')) : ?>


<?php while (have_rows('expandable_section')) :  the_row(); ?>
<section id="<?php echo sanitize_title(get_sub_field('content_title')); ?>" <?php echo sanitize_title(get_sub_field('content_title')) ? 'has-intro' : '' ?> ">
    <div>
        <div>
            <div class="support-label">
                <h1><span class="icon"></span><?php the_sub_field('content_title'); ?></h1>
                <?php if ($excerpt = get_sub_field('content_excerpt')) : ?>
                <div class="intro will-fadeout">
                    <p><?php echo $excerpt; ?></p>
                </div>
                <?php endif; ?>
            </div>

        </div>
    </div>

    <div class=>
        <div class="support-content">
            <div>
                <p><?php the_sub_field('content'); ?> </p>
            </div>

            <?php if (get_sub_field('sidebar_content')) :?>
            <div class="medium-4 columns">
                <?php the_sub_field('sidebar_content'); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php endwhile; endif; ?>
