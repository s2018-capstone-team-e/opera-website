<?php

get_header(); ?>

<!-- Begin Page -->
<?php get_template_part('partial-hero'); ?>

<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1><?php the_title(); ?> </h1>
            <p class="intro"><?php the_field("intro"); ?></p>
        </div>
    </div>
</section>

<section id="po-contact" class="section po-contact">
    <div class="row">
        <div class="medium-4 columns">
            <article class="group">
                <h4>Administration</h4>
                    <?php if (have_rows("administration")): ?>
                    <?php while (have_rows("administration")): the_row(); ?>
                    <div class="venue-details">
                        <address>
                            <strong><?php the_sub_field("name"); ?></strong> <br>
                            <?php the_sub_field("address"); ?>
                        </address>

                        <?php if (get_sub_field("phone_1")): ?>
                        <a href="tel:<?php the_sub_field("phone_1"); ?>" class="phone"><?php the_sub_field("phone_1"); ?></a> <br>
                        <?php endif; ?>

                        <?php if (get_sub_field("phone_2")): ?>
                        <a href="tel:<?php the_sub_field("phone_2"); ?>" class="phone"><?php the_sub_field("phone_2"); ?></a> <br>
                        <?php endif; ?>

                        <?php if (get_sub_field("fax")): ?>
                        Fax: <a href="tel:<?php the_sub_field("fax"); ?>" class="phone"><?php the_sub_field("fax"); ?></a>

                        <?php endif; ?>
                    </div>

                    <?php endwhile; ?>
                    <?php endif; ?>
            </article>

        </div>
        <div class="medium-4 columns">

            <article class="group">
                <h4>Box Office</h4>
                <a href="tel:<?php the_field("box_office_phone_1"); ?>" class="phone"><?php the_field("box_office_phone_1"); ?></a><br>
                <a href="tel:<?php the_field("box_office_phone_2"); ?>" class="phone"><?php the_field("box_office_phone_2"); ?></a><br>
            </article>

            <article class="group">
                <h4>Hours</h4>
                <p><?php the_field("hours"); ?></p>
            </article>

        </div>
        <div class="medium-4 columns">

            <h4>Departments &amp; Staff</h4>

            <?php if (have_rows("department_&_staff")): ?>
            <?php while (have_rows("department_&_staff")): the_row(); ?>
            <article class="group">
            <strong><?php the_sub_field("name"); ?></strong> <br>
                <a href="tel:<?php the_sub_field("phone"); ?>" class="phone"><?php the_sub_field("phone"); ?></a><br>
                <a href="mailto:<?php the_sub_field("email"); ?>"><?php the_sub_field("email"); ?></a>
            </article>
            <?php endwhile; ?>
            <?php endif; ?>

            <a href="/about/staff-directory" class="btn-cta">View Staff Directory &raquo;</a>
        </div>
    </div>

</section>







        <!-- End Page -->

<?php get_footer(); ?>
