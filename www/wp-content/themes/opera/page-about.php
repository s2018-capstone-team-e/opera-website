<?php

get_header(); ?>

<!-- Begin Page -->

<?php get_template_part('partial-hero'); ?>


<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1><?php the_title() ?></h1>
            <p class="intro"><?php the_field("intro"); ?></p>

                <a href="<?php echo get_permalink(get_page_by_path('contact')); ?>" class="button">Contact us</a>
        </div>
    </div>

</section>


<section id="leadership-staff" class="has-background section leadership-staff clearfix light-on-dark">


    <section class="one-half left">
        <div class="inner-wrapper">
            <h2>Leadership</h2>

            <?php if (have_rows("staff_members")): ?>
            <ul class="directors">
                <?php while (have_rows("staff_members")): the_row(); ?>
                <li>
                    <img src="<?php $image = get_sub_field("image"); echo $image['sizes']['leadership-image']; ?>" alt="">
                    <h5><?php the_sub_field("name"); ?></h5>
                    <p><?php the_sub_field("title"); ?></p>
                    <?php if (get_sub_field("link")): ?>
                    <a href="<?php the_sub_field("link"); ?>" class="more">Read More &raquo;</a>
                    <?php endif; ?>
                </li>
                <?php endwhile; ?>
            </ul>
            <?php endif; ?>
        </div>
    </section>
    <section class="one-half right">
        <div class="inner-wrapper">
            <h2>Staff</h2>
            <p><?php the_field("leadership_&_staff_content"); ?></p> <br>
            <a href="<?php echo home_url('/staff/'); ?>" class="more">View staff &raquo;</a> <br>
            <a href="<?php echo home_url('/about/board-governance/'); ?>">Board &amp; Governance &raquo;</a>
        </div>
    </section>
</section>


<section id="po-chorus-orchestra" class="section po-chorus-orchestra">

<div class="intro-image"><img src="<?php $image = get_field("orchestra_image"); echo $image['sizes']['hero']; ?>" alt=""></div>

    <div class="row section-intro">
        <div class="medium-8 medium-centered columns">
            <h2>Portland Opera Chorus &amp; Orchestra</h2>
            <p><?php the_field("chorus_&_orchestra_content"); ?></p>
            <a href="<?php the_field("chorus_orchestra_link"); ?>" class="button">Chorus &amp; Orchestra</a>
        </div>
    </div>


</section>

<section id="po-history" class="section po-history has-rule">

<?php if (get_field("history_image")): ?>
<div class="intro-image"><img src="<?php $image = get_field("history_image"); echo $image['sizes']['hero']; ?>" alt=""></div>
<?php endif; ?>

    <div class="row section-intro">
        <div class="medium-8 medium-centered columns">
            <h2>History</h2>
            <p><?php the_field("history_content"); ?></p>
            <a href="<?php the_field("history_link"); ?>" class="button">Read More</a>
        </div>
    </div>


</section>

<section id="employment-auditions" class="section">
    <div class="row section-intro">
        <div class="medium-8 medium-centered columns">
            <h2>Employment &amp; Auditions</h2>
            <p><?php the_field("employment_&_auditions_content"); ?></p>
            <a href="<?php the_field("employment_auditions_link"); ?>" class="button">Read More</a>
        </div>
    </div>
</section>




        <!-- End Page -->

<?php get_footer(); ?>
