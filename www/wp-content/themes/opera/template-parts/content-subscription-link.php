<?php
if ($production_type == 'broadway') {
    if (get_field('show_broadway_subscription_link', 'option')) {
        echo '<a class="subscribe-link" href="' . get_field('subscribe_link_broadway', 'option') .'">Get Subscription  </a>';
    }
} else {
    if (get_field('show_subscription_link', 'option')) {
        echo '<a class="subscribe-link" href="' . get_field('subscribe_link', 'option') .'">Get Subscription </a>';
    }
}
