<ul class="production-count medium-block-grid-3">

  <?php if ($query->have_posts()): ?>
  <?php while ($query->have_posts()): $query->the_post(); ?>
  <?php if (is_front_page()) {
    if (false == calendar_event_get_next_timestamp(get_the_id())) {
        continue;
    }
} ?>
  <li class="production-obj is-opera">
      <div class="image-wrapper">
          <a href="<?php the_permalink(); ?>" class="thumbnail">

          <?php if ($image = get_field('detail_image')): ?>
            <img src="<?php echo $image['sizes']['season']; ?>">
          <?php endif; ?>
          </a>

          <div class="details">
              <a href="<?php the_permalink(); ?>" class="view-more">View More &raquo;</a>
          </div>
      </div>

      <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
  </li>
  <?php endwhile; ?>
  <?php endif; ?>
  <?php wp_reset_postdata(); ?>
</ul>