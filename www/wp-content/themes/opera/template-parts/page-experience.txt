/*
          FOR THE EXPERIENCE PAGE
   */
  #exper {
    margin: auto;
    width: 52%;
    padding: 10px;
    padding-bottom: 30px;
  }

  #exper h1{
  text-align: center;
  }
  #right {
    display: grid;
    grid-template-columns: auto auto auto;
    grid-column-gap: 40px;
    margin-left: 50px;
    margin-right: 50px;
  }

  #right h3{
    text-align: center;
  }

.thumbnail img {
  border-left-width: 2px;
  border-left-style: solid;
  border-bottom-width: 2px;
  border-bottom-style: solid;
  border-right-width: 2px;
  border-right-style: solid;
  border-top-width: 2px;
  border-top-style: solid;
  padding: 4px;
  max-width: 100px;
  height: auto;
}

.thumbnail img:hover {
  box-shadow: 0 0 3px 2px grey;
}

  #venues-partners  {
    text-align: center;
  }

  #venues-partners p {
    text-align: left;
  }

  #venues-partners {
    display: grid;
    grid-template-columns: auto auto;
    grid-column-gap: 40px;
    margin-left: 50px;
    margin-right: 50px;
    margin-top: 0px;
  }

  #venues-partners h2{
    text-align: center;
  }

.spon-list {
  display: flex;

  align-items: center;
}

.Spon-thumb {
  padding-right: 40px;
}
/* comment out or delete previous declaration of this on line 2661 */
.restaurant-partners, .hotel-partners {
  margin-top: 40px;
  margin-left: 50px;
  margin-right: 50px;
}

#spon {
  margin-bottom: 50px;
}
  /*      END OF EXPER PAGE   */