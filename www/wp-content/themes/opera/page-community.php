<?php

get_header(); ?>

<!-- Begin Page -->

<?php $image = get_field('classes_image'); ?>
<div class="intro-image"><img src="<?php echo $image['sizes']['hero']; ?>" alt=""></div>

<section id="introduction" class="introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1><?php the_title(); ?> </h1>
            <p class="intro"><?php the_field("intro"); ?></p>
        </div>
    </div>

</section>


<section id="po-to-go" class="po-to-go section">
    <div class="intro-image"><img src="<?php $image = get_field("opera_to_go_image"); echo $image['sizes']['hero']; ?>" alt=""></div>

    <div class="row section-intro">
        <div class="medium-8 medium-centered columns">

            <h2>Portland Opera To Go</h2>
            <p><?php the_field("opera_to_go_content"); ?></p>
        </div>
    </div>

    <div class="row sub-sections">
        <div class="medium-10 medium-centered columns">
            <div class="row">
                <div class="medium-6 columns">

                    <h3>Public Performances</h3>
                    <p><?php the_field("public_performances"); ?></p>
                    <?php if (get_field('performance_details_url')): ?>
                    <a href="<?php the_field("performance_details_url"); ?>" class="performance-details"><span class="icon"></span>View Performance Details &raquo;</a>
                    <?php endif; ?>
                </div>
                <div class="medium-6 columns">

                    <h3>School Performances</h3>
                    <p><?php the_field("school_performances"); ?></p>

                </div>
                <div class="medium-6 columns">
                    <h3>Opera Film Series</h3>
                    <p><?php the_field("opera_film_series"); ?></p>
                    <a href="<?php the_field("opera_film_series_link"); ?>">Opera Films Series Here &raquo;</a>

                </div>

                <div class="medium-6 columns">
                    <h3>Radio Broadcasts</h3>
                    <p><?php the_field("radio_broadcasts"); ?></p>
                    <a href="<?php the_field("radio_broadcasts_link"); ?>"><?php the_field("radio_broadcasts_link_text"); ?> &raquo;</a>

                </div>
            </div>
            <div class="launch-gallery">
                <a href="#" data-reveal-id="photo-gallery"  class="button btn-photos">Photos</a>
            </div>

        </div>

    </div>

</section>



</section>


<section id="resident-artists" class="section resident-artists">

    <div class="row section-intro">
        <div class="medium-10 medium-centered columns">
            <h2>Resident Artist Community Performances</h2>
            <p><?php the_field("resident_artist_excerpt"); ?></p>

            <?php $image = get_field('resident_artist_image'); ?>
            <img src="<?php echo $image['sizes']['hero']; ?>" alt="">

            <div class="two-columns ra-story">
                    <?php the_field("resident_artist_content"); ?>
            </div>


            <?php
                $time = current_time('timestamp');

                $args = array(
                    'post_type' => 'event',
                    /*'meta_key' => 'production_times_%_date_&_time',
                    'orderby' => 'meta_value', */
                    'order' => 'ASC',
                    /*'meta_query' => array(
                        array(
                            'key' => 'production_times_%_date_&_time',
                            'value' => $time,
                            'compare' => '>=',
                        )
                    ),*/
                );
                $query = new WP_Query($args);
            ?>

            <!-- Events widget -->
            <div class="events-widget event-category">
                <h3>Upcoming Events</h3>
                <!-- <a href="" class="calendar-link">Calendar</a> -->
                <ul class="event-list">
                    <?php if ($query->have_posts()): ?>
                    <?php while ($query->have_posts()): $query->the_post(); ?>

                    <?php if (have_rows("production_times")) {
                while (have_rows("production_times")) {
                    the_row();
                    $date = date("n/j/y", strtotime(get_sub_field("date_&_time")));
                    break;
                }
            } ?>
                    <li class="odd"><span class="date"><?php echo $date; ?></span><a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a><a href="<?php the_permalink(); ?>" class="btn-more">More Info &raquo;</a></li>
                    <?php endwhile; ?>
                    <?php endif; wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
    </div>
</section>


<div class="row purchase-methods">
    <div class="medium-10 medium-centered columns">
        <div class="row">

            <div class="medium-6 columns">
                <h3 style="height: 70px;">Rush Tickets & Student Dress Rehersal</h3>
                <p style="font-family: Georgia !important;"><?php the_field("rush_tickets_content"); ?></p>

            </div>
            <div class="medium-6 columns">
                <h3 style="height: 70px;">Family Guide To Mainstage Opera</h3>
                <p><?php the_field("family_guide_excerpt"); ?></p>
                <p><?php the_field("family_guide_content"); ?></p>

            </div>
        </div>
    </div>

</div>
<section id="po-classes" class="section po-classes">


    <div class="row section-intro">
        <div  class="medium-8 medium-centered columns">
            <h2>Classes</h2>
        </div>

    </div>
    <div class="row sub-sections">
        <div class="medium-10 medium-centered columns">
            <?php if (have_rows("classes")): ?>
            <ul class="medium-block-grid-1">
                <?php while (have_rows("classes")): the_row(); ?>
                <li>
                    <h3 style="text-align: center"><?php the_sub_field("label"); ?></h3>
                    <p><?php the_sub_field("content"); ?></p>
                    <a href="<?php the_sub_field("link_url"); ?>"><?php the_sub_field("link_label"); ?></a>
                </li>
                <?php endwhile; ?>
            </ul>
            <?php endif; ?>
        </div>
    </div>


</section>



<!-- Modal Gallery content -->
<div id="photo-gallery" class="reveal-modal gallery medium" data-reveal>
    <ul class="gallery-slider" data-orbit data-options="variable_height:true;">
        <?php $photos = get_field("photos"); ?>
        <?php $i = 0; foreach ($photos as $photo): $i++; ?>
        <li>
            <img src="<?php echo $photo['sizes']['gallery-preview']; ?>" alt="slide <?php echo $i; ?>" />
        </li>
        <?php endforeach; ?>
    </ul>
  <a class="close-reveal-modal">&#215;</a>
</div>

        <!-- End Page -->

<?php get_footer(); ?>
