<?php

/*
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 */


/*-----------------------------------------------------------------------------------*/
/* Custom Javascript
/*-----------------------------------------------------------------------------------*/
add_action('wp_print_scripts', 'rly_enqueue_media');
define('ACF_EARLY_ACCESS', '5');

function rly_enqueue_media()
{
    if (!is_admin())
        if (is_singular('venue'))
            wp_enqueue_script('google-map', 'https://maps.googleapis.com/maps/api/js?vs=3.exp&sensor=false', '', '', true);
}


/*-----------------------------------------------------------------------------------*/
/* Custom Plugin Dependencies  */
/*-----------------------------------------------------------------------------------*/

include_once locate_template('plugins.php');    // Plugin Dependencies

/*-----------------------------------------------------------------------------------*/


function calendar_event_get_next_timestamp($event_id, $compare_to = null)
{
    $post_type = get_post_type($event_id);
    $compare_to = $compare_to ?: time();

    $the_next_timestamp = null;

    if (in_array($post_type, array('production', 'event'))) {
        if (have_rows("production_times", $event_id)) {
            $event_timestamps = array();

            while (have_rows("production_times", $event_id)) {
                the_row();

                $event_timestamp = (int) strtotime(get_sub_field("date_&_time"));

                // only add times that are past the $compare_to
                if ($event_timestamp >= $compare_to) {
                    array_push($event_timestamps, $event_timestamp);
                }
            }

            if (!empty($event_timestamps)) {
                $the_next_timestamp = min($event_timestamps);
            }
        }
    }

    return $the_next_timestamp;
}

// returns a pre or post show time that's connected to a given production time
function calendar_event_get_pre_post($event_id, $type, $compare_to = null)
{
    $post_type = get_post_type($event_id);
    $compare_to = $compare_to ?: time();

    $the_next_timestamp = null;

    if (in_array($post_type, array('production', 'event'))) {
        if (have_rows("production_times", $event_id)) {
            $event_timestamps = array();

            while (have_rows("production_times", $event_id)) {
                the_row();

                $event_timestamp = (int) strtotime(get_sub_field($type));
                $prod_timestamp = (int) strtotime(get_sub_field("date_&_time"));

                // only add times that are past the $compare_to
                if ($prod_timestamp == $compare_to) {
                    array_push($event_timestamps, $event_timestamp);
                }
            }

            if (!empty($event_timestamps)) {
                $the_next_timestamp = min($event_timestamps);
            }
        }
    }

    return $the_next_timestamp;
}



function calendar_meta_query($where)
{
    $where = str_replace("meta_key = 'production_times_%_date_&_time'", "meta_key LIKE 'production_times_%_date_&_time'", $where);

    return $where;
}
add_filter('posts_where', 'calendar_meta_query');

function format_phone($phone)
{
    return "tel:".str_replace("-", "", $phone);
}



function opera_pagination()
{
    if (is_singular()) {
        echo "singular";
        return;
    }
    global $wp_query;

    if ($wp_query->max_num_pages <= 1) {
        return;
    }

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max   = intval($wp_query->max_num_pages);

    if ($paged >= 1) {
        $links[] = $paged;
    }

    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    if (get_previous_posts_link()) {
        printf('<li>%s</li>' . "\n", get_previous_posts_link());
    }

    if (! in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (! in_array(2, $links)) {
            echo '<li>…</li>';
        }
    }

    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    if (! in_array($max, $links)) {
        if (! in_array($max - 1, $links)) {
            echo '<li>…</li>' . "\n";
        }

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    if (get_next_posts_link()) {
        printf('<li>%s</li>' . "\n", get_next_posts_link());
    }

    echo '</ul></div>' . "\n";
}

function getDateRange($ID)
{
    if (have_rows("production_times", $ID)) {
        $years = array();
        $dates = array();
        while (have_rows("production_times", $ID)) {
            the_row();
            $day = date("j", strtotime(get_sub_field("date_&_time", $ID)));
            $month = date("F", strtotime(get_sub_field("date_&_time", $ID)));
            $year = date("Y", strtotime(get_sub_field("date_&_time", $ID)));

            if (!is_array($dates[$year][$month])) {
                $dates[$year][$month] = array();
            }

            array_push($dates[$year][$month], $day);

            array_push($years, $year);
        }
        $years = array_unique($years);
        $productionYear = implode("-", $years);
        $productionDates = "";

        foreach ($dates as $year => $months) {
            foreach ($months as $month => $days) {
                if (sizeof($days) > 1) {
                    $range[0] = reset($days);
                    $range[1] = end($days);
                } else {
                    $range = $days;
                }

                $productionDates .= $month . " " . (implode("-", $range)) . ", $year <br/>";
            }
        }
    }

    return $productionDates;
}

function hasProductionTimes($ID)
{
    $hasFields = false;

    if (have_rows("production_times")) {
        while (have_rows("production_times")) {
            the_row();
            if (get_sub_field("date_&_time") != "") {
                $hasFields = true;
            }
        }
    }

    return $hasFields;
}

function calendar_filters_query($query)
{
    if (is_admin()) {
        return $query;
    }

    if (!$query->is_main_query()) {
        return $query;
    }

    if ($query->is_tax('event-category') || $query->is_tax('production-type')) {
        // in these cases, always show the calendar template
        add_filter('template_include', 'opera_redirect_to_calendar_template');
    }
}

add_filter('pre_get_posts', 'calendar_filters_query');

function opera_redirect_to_calendar_template($template)
{
    return get_stylesheet_directory() . '/page-calendar.php';
}

// Add footer scripts
add_action('wp_enqueue_scripts', function(){
  foreach([
      'foundation' => ['https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/js/foundation.min.js', ['jquery'], '6.4.3'],
      'flexslider' => ['https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider.min.js', ['jquery'], '2.7.1'],
      'modernizr' => [get_stylesheet_directory_uri() . '/js/vendor/modernizr.min.js', ['jquery'], false],
      'opera_plugins' => [get_stylesheet_directory_uri() . '/js/plugins.js', ['foundation'], null],
      'opera_site' => [get_stylesheet_directory_uri() . '/js/site.js', ['opera_plugins'], null],
      ] as $name => $info) {
          wp_enqueue_script($name, $info[0], $info[1], $info[2], true);
  }
});

// Add styles
add_action('wp_enqueue_scripts', function(){
  foreach([
      'normalize' => [get_stylesheet_directory_uri() . '/css/normalize.min.css', [], null],
      'foundation' => [get_stylesheet_directory_uri() . '/css/foundation.min.css', ['normalize'], null],
      'opera_style' => [get_stylesheet_directory_uri() . '/css/style.min.css', ['foundation'], false],
      'flexslider' => ['https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css', ['opera_style'], '2.7.1'],
      ] as $name => $info) {
          wp_enqueue_style($name, $info[0], $info[1], $info[2]);
  }
});

// Add google analytics tracking to head instead of footer for more accurate metrics
add_action('wp_head', function(){ ?>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', '<?php the_field('google_analytics_id', 'option'); ?>', 'auto');
          ga('send', 'pageview');

        </script> <?php
}, 20);
