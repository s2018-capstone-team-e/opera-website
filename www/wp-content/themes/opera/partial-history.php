<?php if (have_rows('history_timeline')): ?>
<section id="timeline">


<?php while (have_rows('history_timeline')): the_row(); ?>
<div class="row">
    <div class="medium-centered medium-7 columns">
        <div class="row">
            <div class="columns medium-2"><h4><?php the_sub_field('time_span'); ?></h4></div>
            <div class="columns medium-10 history-summary"><p><?php the_sub_field('event_summary'); ?></p></div>
        </div>
    </div>



</div>

<?php endwhile ?>


</section>

<?php endif; ?>