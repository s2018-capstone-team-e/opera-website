<?php

get_header(); ?>

<section id="introduction" class="page-intro introduction section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1>News &amp; Announcements</h1>
        </div>
    </div>

</section>

<?php

    global $wp_query;
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
    query_posts(array('post_type' => 'post', 'posts_per_page' => '20', 'paged' => $paged ));

?>

    <?php if ($wp_query->have_posts()): ?>
        <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>

        <section class="section has-rule">
            <div class="row">

                <div class="medium-8 medium-centered columns">
                    <article id="post-id" class="post-item entry-content">
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <div class="excerpt-content group">
                            <p><?php the_excerpt(); ?></p>
                        </div>

                        <ul class="post-meta">
                            <li class="date"><?php the_time('F j, Y') ?></li>
                            <li class="read-more"><a href="<?php the_permalink(); ?>" class="more">Read More &raquo;</a></li>
                        </ul>

                    </article>
                </div>
            </div>
        </section>



        <?php endwhile; ?>
    <?php endif; ?>

<section id="pagination" class="pagination section">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <?php opera_pagination(); ?>
        </div>
    </div>
</section>


<?php get_footer(); ?>
