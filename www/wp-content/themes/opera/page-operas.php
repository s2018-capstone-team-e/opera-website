<?php

get_header(); ?>

<!-- Begin Page -->
<?php get_template_part('partial-hero'); ?>


<section id="introduction" class="section dark-on-light introduction">
    <div class="row">
        <div class="medium-8 medium-centered columns">
            <h1><?php the_title(); ?></h1>
            <p class="intro"><?php the_field("intro"); ?></p>
            <?php if (get_field('show_subscription_link', 'option')): ?>
            <a href="<?php the_field("subscribe_link", "option"); ?>" class="button">Subscribe to This Season</a>
            <?php endif; ?>
        </div>
    </div>
</section>

<section id="upcoming" class="upcoming-production section has-rule">

    <?php
        $current = get_field('current_season', 'option');
        $args = array(
            'post_type' => 'production',
            'production-type' => 'opera',
            /*'meta_key' => 'production_times_%_date_&_time',
            'orderby' => 'meta_value',*/
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'season' => $current->name,
        );
        $query = new WP_Query($args);

        // finding out which opera is coming up the soonest by calculating how close
        // the next performance is to the current timestamp.
        $diff = 999999999999999;
        while ($query->have_posts()) {
            $query->the_post();
            $current = calendar_event_get_next_timestamp(get_the_ID()) - time();

            clog($current, get_the_title());
            if ($current < $diff && $current > 0) {
                $upcoming = get_post(get_the_ID());
                $diff = $current;
            }
        }

    ?>

    <div class="row">
        <div class="medium-12 columns">
            <h2 class="has-rule">Upcoming Opera <a href="<?php the_field('tickets_url', $upcoming->ID); ?>" class="btn-more">Get Tickets &raquo;</a></h2>
        </div>
    </div>

    <div class="row">
        <div class="medium-12 columns">

            <div class="production-obj is-large is-opera is-horizontal">
                <a href="<?php the_permalink($upcoming->ID); ?>" class="thumbnail">
                    <img src="<?php $image = get_field("hero", $upcoming->ID); echo $image['sizes']['hero']; ?>">
                </a>

                <div class="details<?php if (!get_field("tickets_url", $upcoming->ID) || null == calendar_event_get_next_timestamp($upcoming->ID)): ?> no-tickets<?php endif; ?>">
                    <a href="<?php echo get_permalink($upcoming->ID); ?>" class="view-more">View more &raquo;</a>
                    <?php if (get_field("tickets_url", $upcoming->ID)): ?>
                    <a href="<?php the_field('tickets_url', $upcoming->ID); ?>" class="tickets">Tickets &raquo;</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</section>

<section id="season-lineup" class="operas-lineup section">
    <div class="row">
        <div class="medium-12 columns">
            <?php $currentSeason = get_field('current_season', 'option'); ?>
            <h2 class="has-rule"> <?php echo $currentSeason->name; ?> Season
                <?php if (get_field('show_subscription_link', 'option')): ?>
                <a href="<?php the_field("subscribe_link", "option"); ?>" class="btn-more">Season Tickets &raquo;</a><!-- Ticketmaster URL  -->
                <?php endif; ?>
            </h2>

            <ul class="medium-block-grid-2">
                <?php if ($query->have_posts()): ?>
            <?php while ($query->have_posts()): $query->the_post(); ?>
            <li class="production-obj is-opera">
                <a href="<?php the_permalink(); ?>" class="thumbnail">
                    <?php the_post_thumbnail('opera-small'); ?>
                </a>

                <div class="details<?php if (!get_field("tickets_url") || null == calendar_event_get_next_timestamp($theID)): ?> no-tickets<?php endif; ?>">
                    <a href="<?php the_permalink(); ?>" class="view-more">View more &raquo;</a>
                    <?php if (null != calendar_event_get_next_timestamp($theID) && get_field("tickets_url", $theID)): ?>
                    <a href="<?php the_field('tickets_url', $theID); ?>" class="tickets">Tickets &raquo;</a>
                    <?php endif; ?>
                </div>
            </li>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>
            </ul>

        </div>
    </div>
</section>


<section id="ticket-info" class="expandable-content has-intro open ticket-info">
    <div class="panel">
        <div class="row">
            <div class="medium-12 columns">
                <h2><span class="icon"></span>Ticket Information</h2>
                <div class="intro will-fadeout">
                    <ul class="link-list is-inline">
                        <?php if (get_field('show_subscription_link', 'option')): ?>
                        <li><a href="<?php the_field("subscribe_link", "option"); ?>">Subscribe or Renew &raquo;</a></li><!-- Ticketmaster URL -->
                        <?php endif; ?>
                        <li><a href="<?php the_field("individual_ticket_link"); ?>">Individual Tickets &raquo;</a></li> <!-- Ticketmaster URL -->
                        <li><a href="<?php the_field("ticket_policy_link"); ?>">Ticket Policies &raquo;</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>

    <div class="inner-content">
        <section class="two-thirds left">
            <div class="inner-wrapper">
                <h2>Ticket Information</h2>
                <p><?php echo get_field('ticket_content'); ?></p>
            </div>

        </section>

        <section class="one-third right">
            <div class="inner-wrapper">
                <ul class="link-list">
                    <?php if (get_field('show_subscription_link', 'option')): ?>
                    <li><a href="<?php the_field("subscribe_link", "option"); ?>">Subscribe or Renew &raquo;</a></li><!-- Ticketmaster URL -->
                    <?php endif; ?>
                    <li><a href="<?php the_field("individual_ticket_link"); ?>">Individual Tickets &raquo;</a></li> <!-- Ticketmaster URL -->
                    <li><a href="<?php the_field("ticket_policy_link"); ?>">Ticket Policies &raquo;</a></li>
                </ul>
            </div>
        </section>
        <section class="two-thirds left">
            <div class="inner-wrapper">
                <p><strong>The Hampton Opera Center</strong> <br>
                        <?php the_field('address', 'option'); ?>

                        <?php if (have_rows('phone_numbers', 'option')): $i = 0; ?>
                        <p>
                        <?php while (have_rows('phone_numbers', 'option')): the_row(); ?>
                        <?php the_sub_field('display_phone_number'); ?><?php echo($i == 0 ? " or " : ""); ?>
                        <?php $i++; endwhile; ?>
                        </p>
                        <?php endif; ?>

                        <p class="hours"><?php the_field('hours', 'option'); ?></p>
            </div>

        </section>
<!--         <div class="row">
            <div class="medium-12 columns">
                <p>Content!!!</p>
            </div>
        </div>-->
    </div>
</section>









        <!-- End Page -->

<?php get_footer(); ?>
