<?php
// For all themes, allowing Admins to Edit Templates / Plugins is a no-no
define('DISALLOW_FILE_EDIT', true);


// custom constant (opposite of STYLESHEETPATH)
define('_TEMPLATEURL', get_bloginfo('stylesheet_directory'));

// boolean check for whether or not this is a dev environment
function is_dev_environment()
{
    if (isset($_ENV['SITE_ENV']) && in_array($_ENV['SITE_ENV'], array('dev', 'staging'))) {
        return true;
    }
    if ('portlandopera.dev' == $_SERVER['SERVER_NAME']) {
        return true;
    }

    return false;
}

if (!is_dev_environment()) {
    define('DISALLOW_FILE_EDIT', true);    // disable file editor
  // define('DISALLOW_FILE_MODS', true);    // disable adding / updating plugins & wordpress
}

// @sxa see: http://wordpress.stackexchange.com/questions/1216/changing-the-order-of-admin-menu-sections
locate_template('inc/wp-admin-menu-classes.php', true);


/* @sxa enable upload mime types */

add_filter('upload_mimes', 'rly_upload_mimes');
function rly_upload_mimes($mime_types)
{
    // Add *.EPS files to Media upload
    $mime_types['eps'] = 'application/postscript';
    $mime_types['zip'] = 'application/zip';
    $mime_types['gz'] = 'application/x-gzip';

    // Add *.AI files to Media upload
    $mime_types['ai'] = 'application/postscript';
    return $mime_types;
}


// Add a separator in the admin to break out the custom content types.
function add_admin_menu_separator($position)
{
    global $menu;
    $index = 0;
    $written  = false;
    foreach ($menu as $offset => $section) {
        if (substr($section[2], 0, 9)=='separator') {
            $index++;
            if ($written == true) {
                $section[2] = substr($section[2], 0, 9) + $index;
            }
        }
        if ($offset>=$position && $written == false) {
            $menu[$position] = array('','read',"separator{$index}",'','wp-menu-separator');
            $written = true;
            break;
        }
    }
    if (is_array($menu)) {
        ksort($menu);
    }
}


/*-----------------------------------------------------------------------------------*/
/* Debug Tools */
/*-----------------------------------------------------------------------------------*/

// @sxa: dump post queries into the error console
add_filter('posts_request', 'dump_request');

function dump_request($input)
{
    error_log($input);

    return $input;
}

/**
 * error logs a variable in a parseable format while maintaining whitespace
 * in html
 * @param $var
 */
function clog($var)
{
    $bt =  debug_backtrace();
    error_log($bt[0]['file'] . ':'. $bt[0]['line'] . "\n" . var_export($var, true));
}
/**
 * Alias for clog
 */
function console_log($var)
{
    return clog($var);
}


// Body class for admin
if (!function_exists('base_admin_body_class')) {
    function base_admin_body_class($classes)
    {
        // Current action
        if (is_admin() && isset($_GET['action'])) {
            $classes .= 'action-'.$_GET['action'];
        }
        // Current post ID
        if (is_admin() && isset($_GET['post'])) {
            $classes .= ' ';
            $classes .= 'post-'.$_GET['post'];
        }
        // New post type & listing page
        if (isset($_GET['post_type'])) {
            $post_type = $_GET['post_type'];
        }
        if (isset($post_type)) {
            $classes .= ' ';
            $classes .= 'post-type-'.$post_type;
        } else {
            $classes .= " post-type-post ";
        }
        // Editting a post type
        $post_query = $_GET['post'];
        if (isset($post_query)) {
            $current_post_edit = get_post($post_query);
            $current_post_type = $current_post_edit->post_type;
            if (!empty($current_post_type)) {
                $classes .= ' ';
                $classes .= 'post-type-'.$current_post_type;
            }
        }
        // Return the $classes array
        return $classes;
    }

    add_filter('admin_body_class', 'base_admin_body_class');
}


// sxa: shortcut for meta values
function meta($the_field, $single=true)
{
    global $post;
    $the_meta = get_post_meta($post->ID, $the_field, $single);
    return $the_meta;
}

// Add all custom post types to the "Right Now" box on the Dashboard
add_action('right_now_content_table_end', 'rly_right_now_content_table_end');

function rly_right_now_content_table_end()
{
    $args = array(
    'show_ui' => true,
    '_builtin' => false
  );
    $output = 'object';
    $operator = 'and';

    $post_types = get_post_types($args, $output, $operator);


    foreach ($post_types as $post_type) {
        $num_posts = wp_count_posts($post_type->name);
        $num = number_format_i18n($num_posts->publish);
        $text = _n($post_type->labels->singular_name, $post_type->labels->name, intval($num_posts->publish));
        if (current_user_can('edit_posts')) {
            $num = "<a href='edit.php?post_type=$post_type->name'>$num</a>";
            $text = "<a href='edit.php?post_type=$post_type->name'>$text</a>";
        }
        echo '<tr><td class="first b b-' . $post_type->name . '">' . $num . '</td>';
        echo '<td class="t ' . $post_type->name . '">' . $text . '</td></tr>';
    }
}


// @sxa copy image directly from url and attach to post
function rly_copy_post_image($url, $post_id)
{
    $time = current_time('mysql');
    if ($post = get_post($post_id)) {
        if (substr($post->post_date, 0, 4) > 0) {
            $time = $post->post_date;
        }
    }

    //making sure there is a valid upload folder
    if (! (($uploads = wp_upload_dir($time)) && false === $uploads['error'])) {
        return false;
    }

    error_log('finding basename in syc post copy:' . $url);
    $name = basename($url);

    $filename = wp_unique_filename($uploads['path'], $name);

    // Move the file to the uploads dir
    $new_file = $uploads['path'] . "/$filename";

    $uploaddir = wp_upload_dir();
    $path = str_replace($uploaddir["baseurl"], $uploaddir["basedir"], $url);

    error_log('copying to: ' . $path);
    if (!copy($path, $new_file)) {
        return false;
    }

    // Set correct file permissions
    $stat = stat(dirname($new_file));
    $perms = $stat['mode'] & 0000666;
    @ chmod($new_file, $perms);

    // Compute the URL
    $url = $uploads['url'] . "/$filename";

    if (is_multisite()) {
        delete_transient('dirsize_cache');
    }

    $type = wp_check_filetype($new_file);
    return array("file" => $new_file, "url" => $url, "type" => $type["type"]);
}

// sxa: hide annoying wordpress dashboard widgets
add_action('admin_init', 'rly_remove_dashboard_widgets');
function rly_remove_dashboard_widgets()
{
    // remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   // right now
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // recent comments
    // remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // incoming links
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // plugins

    remove_meta_box('dashboard_quick_press', 'dashboard', 'normal');  // quick press
    // remove_meta_box('dashboard_recent_drafts', 'dashboard', 'normal');  // recent drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'normal');   // wordpress blog
    remove_meta_box('dashboard_secondary', 'dashboard', 'normal');   // other wordpress news
}

/**
 * Set the latest attachment as the featured image of the given post
 **/
function rly_reset_featured_image($post_id)
{
    // get the last image added to the post
    $attachments = get_posts(array('numberposts' => '1', 'post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC'));


    if (sizeof($attachments) > 0) {
        // set image as the post thumbnail
        set_post_thumbnail($post_id, $attachments[0]->ID);
    }
}

function rly_the_post_thumbnail_caption()
{
    global $post;

    $thumbnail_id    = get_post_thumbnail_id($post->ID);
    $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));

    if ($thumbnail_image && isset($thumbnail_image[0])) {
        echo '<span class="thumbnail-caption">'.$thumbnail_image[0]->post_excerpt.'</span>';
    }
}

function rly_body_classes($classes)
{

//@sxa:  If you don't like the class prefixes I have chosen, you can change them here.
    $post_name_prefix = 'postname-';
    $page_name_prefix = 'pagename-';
    $single_term_prefix = 'single-';
    $single_parent_prefix = 'parent-';
    $category_parent_prefix = 'parent-category-';
    $term_parent_prefix = 'parent-term-';
    $site_prefix = 'site-';

    global $wp_query;
    if (is_single()) {
        $wp_query->post = $wp_query->posts[0];
        setup_postdata($wp_query->post);
        $classes[] = $post_name_prefix . $wp_query->post->post_name;

        $classes[] = $wp_query->post->post_type . '-' . $wp_query->post->post_name;

        $taxonomies = array_filter(get_post_taxonomies($wp_query->post->ID), "is_taxonomy_hierarchical");
        foreach ($taxonomies as $taxonomy) {
            $tax_name = ($taxonomy != 'category') ? $taxonomy . '-' : '';
            $terms = get_the_terms($wp_query->post->ID, $taxonomy);
            if ($terms) {
                foreach ($terms as $term) {
                    if (!empty($term->slug)) {
                        $classes[] = $single_term_prefix . $tax_name . sanitize_html_class($term->slug, $term->term_id);
                    }
                    while ($term->parent) {
                        $term = &get_term((int) $term->parent, $taxonomy);
                        if (!empty($term->slug)) {
                            $classes[] = $single_parent_prefix . $tax_name . sanitize_html_class($term->slug, $term->term_id);
                        }
                    }
                }
            }
        }
    } elseif (is_archive()) {
        if (is_category()) {
            $cat = $wp_query->get_queried_object();
            while ($cat->parent) {
                $cat = &get_category((int) $cat->parent);
                if (!empty($cat->slug)) {
                    $classes[] = $category_parent_prefix . sanitize_html_class($cat->slug, $cat->cat_ID);
                }
            }
        } elseif (is_tax()) {
            $term = $wp_query->get_queried_object();
            while ($term->parent) {
                $term = &get_term((int) $term->parent, $term->taxonomy);
                if (!empty($term->slug)) {
                    $classes[] = $term_parent_prefix . sanitize_html_class($term->slug, $term->term_id);
                }
            }
        }
    } elseif (is_page()) {
        $wp_query->post = $wp_query->posts[0];
        setup_postdata($wp_query->post);
        $classes[] = $page_name_prefix . $wp_query->post->post_name;
    }

    if (is_multisite()) {
        global $blog_id;
        $classes[] = $site_prefix . $blog_id;
    }

    if ($wp_query->post->post_parent == 0) {
        $classes[] = 'is-not-child';
    } else {
        $classes[] = 'is-child';
    }

    return $classes;
}

add_filter('body_class', 'rly_body_classes');

function rly_menu_css_class($css_class, $page)
{
    $css_class[] = "page-slug-" . $page->post_name;
    return $css_class;
}
add_filter("page_css_class", "rly_menu_css_class", 10, 2);


/**
 * Finds out if the current post is the latest published post
 *
 * @global $post
 * @global $wpdb
 * @return bool true if the posts is the latest post, false otherwise
 */
function is_latest($post_type = 'post')
{
    global $post, $wpdb;
    $sql = sprintf(
      "SELECT COUNT(*) FROM %s WHERE post_date > '%s' AND post_type = '%s' AND post_status = 'publish';",
          $wpdb->posts,
          $post->post_date,
          $post_type
  );
    return $wpdb->get_var($wpdb->prepare($sql)) == 0;
}


function has_children()
{
    global $post;

    $pages = get_pages('child_of=' . $post->ID);

    return 0 != count($pages);
}

function has_parent()
{
    global $post;

    if ($post->post_parent) {
        return true;
    } else {
        return false;
    }
}

/**
 * Return a menu name based on a theme location:
 * http://www.andrewgail.com/getting-a-menu-name-in-wordpress/
 */

function rly_get_theme_menu_name($theme_location)
{
    if (! $theme_location) {
        return false;
    }

    $theme_locations = get_nav_menu_locations();
    if (! isset($theme_locations[$theme_location])) {
        return false;
    }

    $menu_obj = get_term($theme_locations[$theme_location], 'nav_menu');
    if (! $menu_obj) {
        $menu_obj = false;
    }
    if (! isset($menu_obj->name)) {
        return false;
    }

    return $menu_obj->name;
}

/*-----------------------------------------------------------------------------------*/
/*   Tag / Whitespace conversion functions
/*-----------------------------------------------------------------------------------*/

// from: http://php.net/manual/en/function.nl2br.php
function strip_replace_tags($str)
{
    return p2nl(br2nl($str));
}

function p2nl($str)
{
    return preg_replace(
        array("/<p[^>]*>/iU","/<\/p[^>]*>/iU"),
                        array("","\n"),
                        $str
    );
}

function br2nl($string)
{
    $return=preg_replace('<br[[:space:]]*/?'.
    '[[:space:]]*>', chr(13).chr(10), $string);
    return $return;
}



/*-----------------------------------------------------------------------------------*/
/* Bogus Content Filters */
/*-----------------------------------------------------------------------------------*/

//add_filter('the_excerpt', 'dummy_excerpt_data');
function dummy_excerpt_data($excerpt)
{
    global $post;
    if (trim(strip_tags($excerpt)) != '&#8230;') {
        echo $excerpt;
    } else {
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cur id non ita fit? An est aliquid, quod te sua sponte delectet&#8230; <a class='read-more' href=''>Read More</a>";
    }
}

function dummy_thumbnail($html, $post_id, $post_thumbnail_id, $size, $attr)
{
    global $_wp_additional_image_sizes;

    $image_sizes = array();

    foreach ($_wp_additional_image_sizes as $image_size => $dimensions) {
        if (!$html && $size == $image_size) {
            $html = "<img src=\"http://placehold.it//{$dimensions[width]}x{$dimensions[height]}\" class=\"attachment-$size wp-post-image\" alt=\"placeholder\" title=\"placeholder\" />";
        }
    }

    //@sxa use default sizes if not custom size (from wp-includes/media.php)
    if (!$html) {
        if ($size == 'thumb' || $size == 'thumbnail') {
            $max_width = intval(get_option('thumbnail_size_w'));
            $max_height = intval(get_option('thumbnail_size_h'));
            // last chance thumbnail size defaults
            if (!$max_width && !$max_height) {
                $max_width = 128;
                $max_height = 96;
            }
        } elseif ($size == 'medium') {
            $max_width = intval(get_option('medium_size_w'));
            $max_height = intval(get_option('medium_size_h'));
        // if no width is set, default to the theme content width if available
        } elseif ($size == 'large') {
            // We're inserting a large size image into the editor. If it's a really
            // big image we'll scale it down to fit reasonably within the editor
            // itself, and within the theme's content width if it's known. The user
            // can resize it in the editor if they wish.
            $max_width = intval(get_option('large_size_w'));
            $max_height = intval(get_option('large_size_h'));
        }
        // last chance thumbnail size defaults
        if (!$max_width && !$max_height) {
            $max_width = 128;
            $max_height = 96;
        }
        $html = "<img src=\"http://placehold.it//{$max_width}x{$max_height}\" class=\"attachment-$size wp-post-image\" alt=\"placeholder\" title=\"placeholder\" />";
    }
    // post_title => image title
    // post_excerpt => image caption
    // post_content => image description

    if ($attachment->post_excerpt || $attachment->post_content) {
        $html .= '<p class="thumbcaption">';
        if ($attachment->post_excerpt) {
            $html .= '<span class="captitle">'.$attachment->post_excerpt.'</span> ';
        }
        $html .= $attachment->post_content.'</p>';
    }

    return $html;
}

add_action('post_thumbnail_html', 'dummy_thumbnail', null, 5);

function sld_unregister_taxonomy_from_object_type($taxonomy, $object_type)
{
    global $wp_taxonomies;
    if (!isset($wp_taxonomies[$taxonomy]) || !get_post_type_object($object_type)) {
        return false;
    }
    foreach (array_keys($wp_taxonomies[$taxonomy]->object_type) as $array_key) {
        if ($wp_taxonomies[$taxonomy]->object_type[$array_key] == $array_key) {
            unset($wp_taxonomies[$taxonomy]->object_type[$array_key]);
            return true;
        }
    }
    return false;
}

/*-----------------------------------------------------------------------------------*/
/* Helper Functions */
/*-----------------------------------------------------------------------------------*/

function ID_by_slug($page_slug)
{
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

// check an array term objects to see if it includes term with $id
function ID_in_objects($objects, $id)
{
    if (is_array($objects)) {
        foreach ($objects as $object) {
            if ($id == $object->term_id) {
                return 'checked';
            }
        }
    }
    return false;
}

function get_url_slug($url)
{
    $path = parse_url($url, PHP_URL_PATH);
    $pathTrimmed = trim($path, '/');
    $pathTokens = explode('/', $pathTrimmed);

    if (substr($path, -1) !== '/') {
        array_pop($pathTokens);
    }

    return end($pathTokens);
}

function maybe_add_http($url)
{
    // skip empty urls
    if (trim($url) == '' || $url === false) {
        return $url;
    }

    if ($url == 'http://') {
        return false;
    }

    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }

    return $url;
}

function rly_get_terms($taxes = array(), $link = true)
{
    global $post, $post_id;
    // get post by post id
    $post = &get_post($post->ID);
    // get post type by post
    $post_type = $post->post_type;
    // get post type taxonomies
    $taxonomies = get_object_taxonomies($post_type);

    foreach ($taxonomies as $taxonomy) {
        if (count((array) $taxes) == 0 || in_array($taxonomy, (array) $taxes)) {
            // get the terms related to post
            $terms = get_the_terms($post->ID, $taxonomy);
            if (!empty($terms)) {
                $out = array();
                foreach ($terms as $term) {
                    if ($link) {
                        $out[] = '<a href="' .get_term_link(intval($term->term_id), $taxonomy) .'">'.$term->name.'</a>';
                    } else {
                        $out[] = $term->name;
                    }
                }

                $return = join(', ', $out);
            }
        }
    }
    return $return;
}

// Less Yoast SEO
function rly_yoast_metabox_bottom()
{
    return 'low';
}
add_filter('wpseo_metabox_prio', 'rly_yoast_metabox_bottom');

// Remove ‘page analysis’ and annoying SEO columns
add_filter('wpseo_use_page_analysis', '__return_false');
