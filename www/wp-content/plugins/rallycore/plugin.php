<?php
/**
 * Plugin Name: Rally Core Functionality
 * Plugin URI: https://github.com/billerickson/Core-Functionality
 * Description: This contains all your site's core functionality so that it is theme independent.
 * Version: 1.0
 * Author: Rally Group
 * Author URI: http://www.rallygroup.com
 *
 */

// Plugin Directory
define('RLY_DIR', dirname(__FILE__));

// General
include_once(RLY_DIR . '/lib/rally.php');
