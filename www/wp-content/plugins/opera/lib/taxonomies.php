<?php
/**
 * Taxonomies
 *
 * This file registers any custom taxonomies
 *
 * @package      Opera_Core
 * @since        1.0.0
 */


/**
 * Create Production Type Taxonomy
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function opera_register_production_type_taxonomy()
{
    $labels = array(
        'name' => 'Production Types',
        'singular_name' => 'Production Type',
        'search_items' =>  'Search Production Types',
        'all_items' => 'All Production Types',
        'parent_item' => 'Parent Production Type',
        'parent_item_colon' => 'Parent Production Type:',
        'edit_item' => 'Edit Production Type',
        'update_item' => 'Update Production Type',
        'add_new_item' => 'Add New Production Type',
        'new_item_name' => 'New Production Type Name',
        'menu_name' => 'Production Type'
    );

    register_taxonomy(
        'production-type',
        array('production'),
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'calendar/production-type' ),
        )
    );

    $default = array("Opera", "Broadway");

    foreach ($default as $term) {
        if (!term_exists(strtolower($term), 'production-type')) {
            wp_insert_term(
                $term,
                'production-type',
                array(
                    'description' => '',
                    'slug' => strtolower($term),
                )
            );
        }
    }
}
add_action('init', 'opera_register_production_type_taxonomy');


/**
 * Create Season Taxonomy
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function opera_register_season_taxonomy()
{
    $labels = array(
        'name' => 'Seasons',
        'singular_name' => 'Season',
        'search_items' =>  'Search Seasons',
        'all_items' => 'All Seasons',
        'parent_item' => 'Parent Season',
        'parent_item_colon' => 'Parent Season:',
        'edit_item' => 'Edit Season',
        'update_item' => 'Update Season',
        'add_new_item' => 'Add New Season',
        'new_item_name' => 'New Season Name',
        'menu_name' => 'Season'
    );

    register_taxonomy(
        'season',
        array('production'),
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'season' ),
        )
    );

    $default = array("2014-2015", "2015-2016");

    foreach ($default as $term) {
        if (!term_exists(strtolower($term), 'season')) {
            wp_insert_term(
                $term,
                'season',
                array(
                    'description' => '',
                    'slug' => strtolower($term),
                )
            );
        }
    }
}
add_action('init', 'opera_register_season_taxonomy');




/**
 * Initialize Category Taxonomy
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function opera_init_event_category_taxonomy()
{
    $labels = array(
        'name'                  => _x('Category', 'Taxonomy Categories', 'text-domain'),
        'singular_name'         => _x('Event Category', 'Taxonomy Category', 'text-domain'),
        'search_items'          => __('Search Categories', 'text-domain'),
        'popular_items'         => __('Popular Categories', 'text-domain'),
        'all_items'             => __('All Categories', 'text-domain'),
        'parent_item'           => __('Parent Category', 'text-domain'),
        'parent_item_colon'     => __('Parent Category', 'text-domain'),
        'edit_item'             => __('Edit Event Category', 'text-domain'),
        'update_item'           => __('Update Category', 'text-domain'),
        'add_new_item'          => __('Add New Category', 'text-domain'),
        'new_item_name'         => __('New Category Name', 'text-domain'),
        'add_or_remove_items'   => __('Add or remove Categories', 'text-domain'),
        'choose_from_most_used' => __('Choose from most used text-domain', 'text-domain'),
        'menu_name'             => __('Categories', 'text-domain'),
    );

    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_admin_column' => true,
        'hierarchical'      => true,
        'show_tagcloud'     => true,
        'show_ui'           => true,
        'query_var'         => true,
        'rewrite' => array( 'slug' => 'calendar/category' ),
        'query_var'         => true,
    );

    register_taxonomy('event-category', array( 'event' ), $args);

    $default = array("Classes", "Community", "Resident Artist", "Film Series");

    foreach ($default as $term) {
        if (!term_exists(strtolower($term), 'event-category')) {
            wp_insert_term(
                $term,
                'event-category',
                array(
                    'description' => '',
                    'slug' => strtolower($term),
                )
            );
        }
    }
}
add_action('init', 'opera_init_event_category_taxonomy');
