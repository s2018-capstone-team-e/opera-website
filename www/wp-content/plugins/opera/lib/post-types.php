<?php
/**
 * Post Types
 *
 * This file registers any custom post types and modifies built in post types
 *
 * @package      Opera_Core
 * @since        1.0.0
 */


function rly_page_metaboxes()
{
    remove_meta_box('postcustom', 'page', 'normal');    //removes custom fields for page
    remove_meta_box('postimage', 'page', 'normal');     //removes custom fields for page
    remove_post_type_support('page', 'thumbnail');
}
add_action('admin_menu', 'rly_page_metaboxes');


/**
 * Create Production post type
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

function opera_register_production_post_type()
{
    $labels = array(
        'name' => 'Productions',
        'singular_name' => 'Production',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Production',
        'edit_item' => 'Edit Production',
        'new_item' => 'New Production',
        'view_item' => 'View Production',
        'search_items' => 'Search Productions',
        'not_found' =>  'No Productions found',
        'not_found_in_trash' => 'No Productions found in trash',
        'parent_item_colon' => '',
        'menu_name' => 'Productions'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => get_template_directory_uri() .'/images/admin/music-beam-16.png',
        'supports' => array('title','thumbnail', 'editor', 'excerpt', 'revisions', 'page-attributes')
    );


    register_post_type('production', $args);

    // setup wordpres admin list view
    add_filter("manage_edit-production_columns", "production_edit_columns");

    function production_edit_columns($columns)
    {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => "Title",
            "production-type" => "Production Type",
            "next-date" => "Next Production Date",
            "date" => "Date",
        );

        return $columns;
    }

    add_action("manage_posts_custom_column", "production_custom_columns");
    function production_custom_columns($column, $post_id=null)
    {
        $post_type = get_post_type($post_id);

        switch ($column) {
          case "production-type":
              $taxonomy = 'production-type';
              $terms = get_the_terms($post_id, $taxonomy);
              if (is_array($terms)) {
                  foreach ($terms as $term) {
                      $post_terms[] = "<a href='edit.php?post_type={$post_type}&{$taxonomy}={$term->slug}'> " . esc_html(sanitize_term_field('name', $term->name, $term->term_id, $taxonomy, 'edit')) . "</a>";
                  }
                  echo join(', ', $post_terms);
              }
              break;
          case "next-date":
            if ($timestamp = calendar_event_get_next_timestamp($post_id)) {
                echo date('M j, Y g:ia', $timestamp);
            } else {
                echo "No upcoming dates.";
            }
      }
    }
}
add_action('init', 'opera_register_production_post_type');

function rly_production_metaboxes()
{
    remove_meta_box('production-typediv', 'production', 'side');    //removes custom fields for page
}
add_action('admin_menu', 'rly_production_metaboxes');


/**
 * Create Event post type
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

function opera_register_event_post_type()
{
    $labels = array(
        'name' => 'Events',
        'singular_name' => 'Event',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Event',
        'edit_item' => 'Edit Event',
        'new_item' => 'New Event',
        'view_item' => 'View Event',
        'search_items' => 'Search Events',
        'not_found' =>  'No Events found',
        'not_found_in_trash' => 'No Events found in trash',
        'parent_item_colon' => '',
        'menu_name' => 'Events'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => get_template_directory_uri() .'/images/admin/calendar-month.png',
        'supports' => array('title','thumbnail', 'editor',),
        'taxonomies' => array('event-category')
    );

    register_post_type('event', $args);



    // setup wordpres admin list view
    add_filter("manage_edit-event_columns", "event_edit_columns");

    function event_edit_columns($columns)
    {
        $offset = 3;
        $new_columns = array_splice($columns, 0, $offset, true) +
        array('next-date'=>'Next Event Date') +
        array_splice($columns, $offset, null, true);

        return $new_columns;
    }
}
add_action('init', 'opera_register_event_post_type');


/**
 * Create Venue post type
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

function opera_register_venue_post_type()
{
    $labels = array(
        'name' => 'Venues',
        'singular_name' => 'Venue',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Venue',
        'edit_item' => 'Edit Venue',
        'new_item' => 'New Venue',
        'view_item' => 'View Venue',
        'search_items' => 'Search Venues',
        'not_found' =>  'No Venues found',
        'not_found_in_trash' => 'No Venues found in trash',
        'parent_item_colon' => '',
        'menu_name' => 'Venues'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => get_template_directory_uri() .'/images/admin/building.png',
        'supports' => array('title', 'editor')
    );

    register_post_type('venue', $args);
}
add_action('init', 'opera_register_venue_post_type');


/**
 * Change rewrite for default `post` type
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

add_action('init', 'register_default_post_type', 1);
function register_default_post_type()
{
    register_post_type('post', array(
        'labels' => array(
            'name_admin_bar' => _x('Post', 'add new on admin bar'),
        ),
        'public'  => true,
        '_builtin' => false,
        '_edit_link' => 'post.php?post=%d',
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'news' ),
        'query_var' => false,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' ),
    ));
}

if (function_exists('p2p_register_connection_type')) {

    // Related Venues (to Productions)
    p2p_register_connection_type(array(
        'name' => 'related_venue_production',
        'from' => 'production',
        'from_query_vars' => array('posts_per_page'=>30),
        'to' => 'venue',
        'to_labels' => array(
          'singular_name' => 'Related Venue',
          'search_items' => 'Search Venue',
          'not_found' => 'No venue found.',
          'create' => 'Create Venue Connection',
        ),

        'title' => array( 'from' => 'Venue', 'to' => 'Related Productions'),
        'reciprocal' => true,
        'sortable' => 'to',
    ));

    p2p_register_connection_type(array(
        'name' => 'related_venue_production',
        'from' => 'production',
        'from_query_vars' => array('posts_per_page'=>30),
        'to' => 'venue',
        'to_labels' => array(
          'singular_name' => 'Related Venue',
          'search_items' => 'Search Venue',
          'not_found' => 'No venue found.',
          'create' => 'Create Venue Connection',
        ),

        'title' => array( 'from' => 'Venue', 'to' => 'Related Productions'),
        'reciprocal' => true,
        'sortable' => 'to',
    ));

    // Related Productions
    p2p_register_connection_type(array(
        'name' => 'related_production',
        'from' => 'production',
        'from_query_vars' => array('posts_per_page'=>30),
        'to' => 'production',
        'to_labels' => array(
          'singular_name' => 'Related Production',
          'search_items' => 'Search Production',
          'not_found' => 'No production found.',
          'create' => 'Create Production Connection',
        ),

        'title' => array( 'from' => 'Related Productions', 'to' => 'Related Productions'),
        'reciprocal' => true,
        'sortable' => 'to',
    ));
}
