<?php

// Gravity forms license key
define('GF_LICENSE_KEY', '24f1938b325d08d1c0735488d5ee80a6');

// Gravity forms default form xml (imports by default)
// http://www.gravityhelp.com/documentation/page/Advanced_Configuration_Options#GF_THEME_IMPORT_FILE

// this should be a path relative to themes/opera
define("GF_THEME_IMPORT_FILE", "../../plugins/opera/gravityforms-export-2014.xml");

function opera_edit_forms_buttons($field_groups)
{
    $new = array(array('name' => 'form_fields', 'label' => 'Form Fields', 'fields' => array()));

    $ignore_groups = array('pricing_fields', 'post_fields');
    $ignore_fields = array('Radio Buttons', 'Time', 'Multi Select', 'Page Break', 'Section Break', 'Radio Button', 'Hidden', 'Website', 'HTML', 'List', 'Number');
    $rename_fields = array('Single Line Text' => 'Short Response', 'Paragraph Text' => 'Long Response', 'Date' => 'Requested Date');

    foreach ($field_groups as $group) {
        if (in_array($group['name'], $ignore_groups)) {
            continue;
        }

        foreach ($group['fields'] as $field) {
            if (in_array($field['value'], $ignore_fields)) {
                continue;
            }
            if (array_key_exists($field['value'], $rename_fields)) {
                $field['value'] = $rename_fields[$field['value']];
            }
            $new[0]['fields'][] = $field;
        }
    }

    return $new;
}
add_filter('gform_add_field_buttons', 'opera_edit_forms_buttons');

function custom_menu_order($menu_ord)
{
    if (!$menu_ord) {
        return true;
    }

    return array(
        'index.php', // Dashboard
        'separator1', // First separator
        'edit.php?post_type=page', // Pages
        'edit.php?post_type=production',
        'edit.php?post_type=event',
        'edit.php?post_type=venue',
        'edit.php?post_type=post',
        'upload.php', // Media
        'separator2', // Second separator
        'themes.php', // Appearance
        'plugins.php', // Plugins
        'users.php', // Users
        'tools.php', // Tools
        'options-general.php', // Settings
        'separator-last', // Last separator
    );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');

function edit_admin_menus()
{
    remove_menu_page('edit.php');
}
add_action('admin_menu', 'edit_admin_menus');


// Admin CSS
function opera_base_admin_css()
{
    wp_enqueue_style('base-admin-css', get_stylesheet_directory_uri() .'/css/admin.css', false, '1.0', 'all');
}
add_action('admin_print_styles', 'opera_base_admin_css');



/*-----------------------------------------------------------------------------------*/
/* Login Screen Customizations */
/*-----------------------------------------------------------------------------------*/

function opera_login_stylesheet()
{
    ?>
    <link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_bloginfo('stylesheet_directory') . '/css/login.css'; ?>" type="text/css" media="all" />
<?php
}
add_action('login_enqueue_scripts', 'opera_login_stylesheet');

function opera_login_logo_url()
{
    return get_bloginfo('url');
}
add_filter('login_headerurl', 'opera_login_logo_url');

function opera_login_logo_url_title()
{
    return get_bloginfo('name');
}
add_filter('login_headertitle', 'opera_login_logo_url_title');

function rly_general_theme_setup()
{

    /*-----------------------------------------------------------------------------------*/
    /* Custom Image Sizes */
    /*-----------------------------------------------------------------------------------*/

    // Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
    add_theme_support('post-thumbnails');

    add_image_size('production-medium', 400, 400, true);

    add_image_size('production-small', 380, 300, true);

    add_image_size('production-featured', 400, 600, true);

    add_image_size('production-sponsor', 400, 200, true);

    add_image_size('production-connection', 50, 50, true);


    // image sizes //
    add_image_size('upcoming-opera', 2000, 800, true);

    add_image_size('season', 380, 300, true);

    add_image_size('current-season', 980, 480);

    add_image_size('opera-small', 490, 240);

    add_image_size('hero', 2200, 1000);

    add_image_size('gallery-preview', 480, 480, true);

    add_image_size('leadership-image', 100, 100);

    if (350 != get_option('thumbnail_size_w')) {
        update_option('thumbnail_size_w', 300);
    }
    if (350 != get_option('thumbnail_size_h')) {
        update_option('thumbnail_size_h', 215);
    }
    if (1 != get_option('thumbnail_crop')) {
        update_option('thumbnail_crop', 1);
    }

    // default thumb size.
    set_post_thumbnail_size(350, 350, true);
}
add_action('after_setup_theme', 'rly_general_theme_setup');

function opera_set_gravity_forms_options()
{
    if (get_option('rg_gforms_enable_html5') != 1) {
        update_option('rg_gforms_enable_html5', 1);
    }
}
add_action('init', 'opera_set_gravity_forms_options');

function add_calendar_rewrite()
{
    add_rewrite_rule('^calendar/([^/]*)/([^/]*)/?', 'index.php?page_id='.ID_by_slug('calendar').'&type=$matches[1]&value=$matches[2]', 'top');
    add_rewrite_tag('%type%', '([^/]*)');
    add_rewrite_tag('%value%', '([^/]*)');
}
add_action('init', 'add_calendar_rewrite');


function rewrite_query_vars($query_vars)
{
    $query_vars[] = 'type';
    $query_vars[] = 'value';

    return $query_vars;
}
add_action('query_vars', 'rewrite_query_vars');


function opera_future_events_object_query($args, $field, $post)
{
    $time = current_time('timestamp');


    // modify criteria for selection
    $args['meta_query'] = array(
        array(
            'key' => 'production_times_%_date_&_time',
            'value' => $time,
            'compare' => '>=',
        )
    );

    return $args;
}

// filter for a specific field based on it's name
add_filter('acf/fields/relationship/query/name=event_widget', 'opera_future_events_object_query', 10, 3);
