<?php
/**
 * Plugin Name: Opera Core Functionality
 * Plugin URI: https://github.com/billerickson/Core-Functionality
 * Description: This contains all your site's core functionality so that it is theme independent.
 * Version: 1.0
 * Author: Rally Group
 * Author URI: http://www.rallygroup.com
 *
 */

// Plugin Directory
define('OPERA_DIR', dirname(__FILE__));

// load functionality plugin after other plugins are loaded.

add_action('plugins_loaded', 'gal_plugin_load');

function gal_plugin_load()
{
    // Functions
    include_once(OPERA_DIR . '/lib/functions.php');

    // Post Types
    include_once(OPERA_DIR . '/lib/post-types.php');

    // Taxonomies
    include_once(OPERA_DIR . '/lib/taxonomies.php');

    // Menus
    include_once(OPERA_DIR . '/lib/menus.php');
}

add_filter('acf/compatibility/field_wrapper_class', '__return_true');
