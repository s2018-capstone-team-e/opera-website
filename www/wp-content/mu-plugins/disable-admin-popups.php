<?php
/*
Plugin Name: Disable Yoast popups & redirects
Plugin URI:
Description: Disables the Yoast popups and redirects for WordPress SEO by Yoast and Yoast Google Analytics. Sorry Joost, it's a huge annoyance dealing with the popups on a daily basis for otherwise awesome plugins!
Author: Daan Kortenbach
Version: 0.1
Author URI: http://daan.kortenba.ch/
*/

add_action('admin_init', 'dmk_disable_yoast_popups', 9999);
/**
 * Disables the Yoast popups. Sorry Yoast, it's a huge annoyance dealing with the popups on a daily basis for an otherwise awesome plugin!
 * @return [type] [description]
 */
function dmk_disable_yoast_popups()
{

    // Disable WordPress SEO annoyances
    if ($dmk_yoast_seo_options = get_option('wpseo')) {
        $update = false;

        if (! isset($dmk_yoast_seo_options['tracking_popup']) or $dmk_yoast_seo_options['tracking_popup'] != 'done') {
            $dmk_yoast_seo_options['tracking_popup'] = 'done';
            $update = true;
        }

        if (! isset($dmk_yoast_seo_options['yoast_tracking']) or $dmk_yoast_seo_options['yoast_tracking'] != 'off') {
            $dmk_yoast_seo_options['yoast_tracking'] = 'off';
            $update = true;
        }
        if (! isset($dmk_yoast_seo_options['ignore_tour']) or $dmk_yoast_seo_options['ignore_tour'] != 'ignore') {
            $dmk_yoast_seo_options['ignore_tour'] = 'ignore';
            $update = true;
        }

        if ($update == true) {
            update_option('wpseo', $dmk_yoast_seo_options);
        }
    } else {
        $dmk_disable_wpseo_popups = array(
            'tracking_popup' => 'done',
            'yoast_tracking' => 'off',
            'ignore_tour' => 'ignore',
        );
        update_option('wpseo', $dmk_disable_wpseo_popups);
    }


    // Disable Yoast Google Analytics annoyances
    if ($dmk_yoast_analytics_options = get_option('Yoast_Google_Analytics')) {
        $update = false;

        if (! isset($dmk_yoast_analytics_options['tracking_popup']) or $dmk_yoast_analytics_options['tracking_popup'] != 'done') {
            $dmk_yoast_analytics_options['tracking_popup'] = 'done';
            $update = true;
        }
        if (! isset($dmk_yoast_analytics_options['yoast_tracking']) or $dmk_yoast_analytics_options['yoast_tracking'] != 'off') {
            $dmk_yoast_analytics_options['yoast_tracking'] = 'off';
            $update = true;
        }

        if ($update == true) {
            update_option('Yoast_Google_Analytics', $dmk_yoast_analytics_options);
        }
    } else {
        $dmk_disable_analytics_popups = array(
            'tracking_popup' => 'done',
            'yoast_tracking' => 'off'
        );
        update_option('Yoast_Google_Analytics', $dmk_disable_analytics_popups);
    }
}

add_filter('http_request_args', 'dmk_disable_yoast_popups_slug_hidden_plugin', 5, 2);
/*
 * Disable plugin updates
 *
 * @param array  $r   Response header
 * @param string $url The update URL
 */
function dmk_disable_yoast_popups_slug_hidden_plugin($r, $url)
{
    if (0 !== strpos($url, 'http://api.wordpress.org/plugins/update-check')) {
        return $r;
    } // Not a plugin update request. Bail immediately.
    $plugins = unserialize($r['body']['plugins']);
    unset($plugins->plugins[ plugin_basename(__FILE__) ]);
    unset($plugins->active[ array_search(plugin_basename(__FILE__), $plugins->active) ]);
    $r['body']['plugins'] = serialize($plugins);
    return $r;
}
