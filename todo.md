1. Get the remote website setup (just do it on my server, whatever)
2. Talk with sponsors about button options
  * https://www.wordstream.com/blog/ws/2015/02/20/call-to-action-buttons
  * https://neilpatel.com/wp-content/uploads/2010/08/color-purchases-lrg.png
3. Focus on A/B testing as a way to sell some of the stuff we're presenting.


1. Progress on slider
  - Documentation
  - Does the slider require more changes?
  - I don't see the gallery picker, for example
2. Figure out what's up with people's commits
  - Weird email?
  - Some people don't have commits?
3. Page progress
  - Everyone needs a branch for the page they're working on
  - Mockups?
  - When people are re-designing their pages, they should see how easy it is to move away from ACF
4. Add redirect for /operas to always go to latest season
  - add page template for redirect to content link
5. Create incremental guide for the team
  - Migrating to shortcodes
  - Moving away from ACF
  - Migrating away from the theme
  - Making short changes to improve things (A/B testing, QA site, etc)
