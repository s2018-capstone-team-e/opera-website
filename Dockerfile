FROM ulsmith/alpine-apache-php7
LABEL creator="PSU Capstone Team E, Spring 2017/2018"

EXPOSE 80
COPY /www /app/public
COPY wp-config.php /app/public
RUN chown -R apache:apache /app
