module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.initConfig({
        watch: {
            files: ['www/wp-content/themes/opera/css/scss/*.scss'],
            tasks: ['sass'],
        },
        sass: {
            dist: {
                options: {
                    style: "compressed"
                },
                files: [{
                    expand: true,
                    cwd: 'www/wp-content/themes/opera/css/scss/',
                    src: ['*.scss'],
                    dest: 'www/wp-content/themes/opera/css/',
                    ext: '.css'
                }],
            },
        },
    });

    grunt.registerTask('default',['watch']);
    grunt.registerTask('build', ['sass']);
};
