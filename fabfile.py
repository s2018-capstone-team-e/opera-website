# -*- coding: utf-8 -*-
# Switchyard Wordpress Fabfile
# Copied from Chicago Tribune News Applications fabfile
# No additional copying allowed

import os
import subprocess

from datetime import datetime
from ConfigParser import SafeConfigParser, Error as ConfigParserError

from fabric.api import *
from fabric.colors import *
from fabric.contrib.console import confirm
from fabric.contrib.files import exists, contains, upload_template, sed
from fabric.context_managers import cd

from getpass import getpass


"""
Load Base configuration & Environments
"""

from fabconfig import *

"""
Commands - setup
"""
def git_clone_repo():
    """
    Do initial clone of the git repository.
    """
    with settings(warn_only=True):
        # see http://stackoverflow.com/questions/5377960/whats-the-best-practice-to-git-clone-into-an-existing-folder
        run('git clone %(gitrepo)s %(path)s/tmp' % env)
        run('mv %(path)s/tmp/.git %(path)s/.git' % env)
        run('rm -rf %(path)s/tmp' % env)

def git_add_remote():
    """
    Run a git init and add remote into an exisiting path
    """
    with settings(warn_only=True):
        run('git init %(path)s' % env)
        with cd(env.path):
            run('git remote add origin %(gitrepo)s' % env)
            run('git config branch.%(gitbranch)s.remote origin' % env)
            run('git config branch.%(gitbranch)s.merge refs/heads/%(gitbranch)s' % env)


def git_checkout():
    """
    Pull the latest code on the specified branch.
    """
    with cd(env.path):
        if env.gitbranch != 'master':
            with settings(warn_only=True):
                if env.fix_perms:
                    fix_perms()
                run('git checkout -b %(gitbranch)s origin/%(gitbranch)s' % env)
                run('git checkout %(gitbranch)s' % env)

        try:
            run('git reset --hard')
        except:
            pass

        try:
            run('git pull origin %(gitbranch)s' % env)
        except:
            print("Opening Shell session.")
            open_shell("cd %(path)s" % env)
        create_htaccess()
        update_git_tag()
        flush_rewrites()


def svn_checkout():
    """
    Checkout the site
    """
    env.svn_user = prompt('SVN Username: ')
    env.svn_pass = getpass('Enter SVN Password: ')

    with cd(env.path):
        run('svn co %(repo)s . --username %(svn_user)s --password %(svn_pass)s' % env)


def update_git_tag():
    env.git_tag = run('cat "%(path)s/.git/FETCH_HEAD"' % env).split("\t")[0].split(' ')[0]
    if not env.git_tag:
        return
    # set env variable that lets us know git_tag
    if not contains("%(path)s/.htaccess" % env, 'SetEnv GIT_TAG'):
        return
    sed("%(path)s/.htaccess" % env, "SetEnv GIT_TAG .*$", 'SetEnv GIT_TAG "%(git_tag)s"' % env)


def add_htaccess_tag():
   # set env variable that lets us distinguish servers
    if not contains("%(path)s/.htaccess" % env, 'SWITCHYARD_ENV'):
        run('echo "\n\n# BEGIN Switchyard \n SetEnv SWITCHYARD_ENV \"%(settings)s\"\n SetEnv GIT_TAG "head"\n# END Switchyard\n\n" >> %(path)s/.htaccess' % env)


def restart_server():
    # Restart Apache
    sudo('apachectl restart')


"""
Commands - deployment
"""
def setup(home_dir="$HOME"):
    """
    Setup the site
    """
    create_db()
    setup_bash_profile(home_dir)
    setup_gitconfig(home_dir)
    copy_key_to_vcs(home_dir)

    git_add_remote()

    if 'beanstalk' in env.gitrepo:
        # Setup beanstalk auto-deploy server
        try:
            import beanstalk
        except ImportError:
            print("Could not find beanstalk module. Try running: $ pip install -r requirements.txt ")
        else:
            config = SafeConfigParser()
            try:
                config.read([os.path.join(os.getenv('HOME'), '.beanstalk')])
                beanstalk.setup(
                    config.get('beanstalk', 'domain'),
                    config.get('beanstalk', 'user'),
                    config.get('beanstalk', 'password'))

            except ConfigParserError:
                print("Your $HOME/.beanstalk file appears to be missing.  Please create it and try again.")
                print("Continuing without Beanstalk integration...")

            except Exception as e:
                print("Beanstalk integration failed: %s" % e)

            else:
                try:
                    bs_repo_id = beanstalk.api.repository.get_id_by_name("%(project_name)s" % env)
                    # create environment
                    bs_env = beanstalk.api.environment.create(bs_repo_id,
                        '%(settings)s-%(project_name)s' % env, automatic=env.automatic_deploy, branch_name=env.gitbranch)

                    # create release server
                    beanstalk.api.releaseserver.create(bs_repo_id, bs_env['server_environment']['id'],
                        '%(settings)s-%(project_name)s-%(host)s' % env, "shell", remote_path=env.path,
                        remote_addr=env.host, port=env.port, login=env.user,
                        shell_code="git pull origin %(gitbranch)s" % env, authenticate_by_key="true")
                except:
                    print("Could not create deployment server in Beanstalk automatically.  Continuing...")

        run('git config --global color.ui true')
        run('git config --global alias.st status')
        run('git config --global alias.ci commit')

    with cd(env.path):
        git_checkout()

        create_wp_config()
        create_htaccess()
        add_htaccess_tag()
        update_git_tag()

    setup_folder_perms()

    if env.fix_perms:
        fix_perms()

    if exists("%(path)s/index.html" % env):
        run("rm index.html")
    local("open http://%(wpdomain)s" % env)


def deploy():
    """
    Deploy new code to the site
    """
    maintenance_up()
    if env.strategy == 'git':
        git_checkout()

    add_htaccess_tag()
    update_git_tag()
    dump_db('backup-deployment-%s-%s' % (env.settings, datetime.strftime(datetime.now(), format="%Y-%m-%d-%H%M")))

    setup_folder_perms()

    if env.fix_perms:
        fix_perms()

    maintenance_down()



def provision_server():
    """
    Setup Apache settings
    """
    with settings(user='root'):

        env.run('cat /etc/apache2/sites-available/template.sysite.co | sed s/projectname/%(wpdomain)s/g > /etc/apache2/sites-available/%(wpdomain)s' % env, shell=False)

        env.run('mkdir -p %(docroot_path)s/www/wp-content/uploads' % env, shell=False)
        env.run('mkdir -p %(docroot_path)s/data' % env, shell=False)

        env.run('chown -R %(user)s:%(apache_group)s %(docroot_path)s' % env, shell=False)
        env.run('chown -R %(apache_user)s:%(apache_group)s %(docroot_path)s/www/wp-content/uploads' % env, shell=False)
        env.run('chmod -R g+rwxs %(docroot_path)s' % env, shell=False)
        env.run('chmod -R 755 %(docroot_path)s/www/wp-content/uploads' % env, shell=False)

        env.run('a2ensite %(wpdomain)s' % env, shell=False)

        env.run('service apache2 reload')




"""
Commands - data
"""
def shell():
    print("Opening Shell session.")
    open_shell("cd %(path)s" % env)

def sql_shell():
    print("Opening SQL Shell session.")
    if not env.db_root_pass:
        env.db_root_pass = getpass("Database password: ")
    open_shell("cd %(path)s; mysql --host=%(db_host)s --user=%(db_root_user)s --password='%(db_root_pass)s' %(db_name)s" % env)


def bootstrap():
    print("\nStep 1: Database and basic Wordpress setup")

    with cd(env.path):
        env.run('cp -P %(config_file)s wp-config.php' % env)

    if env.fix_perms:
        fix_perms()

    create_db()
    env.run('curl -s http://%(wpdomain)s/scripts/na-install.php' % env)

    print("\nStep 2: Setup plugins")

    env.run('curl -s http://%(wpdomain)s/scripts/na-setup-plugins.php' % env)

    print("\nStep 3: Cleanup, create blogs")

    env.run('curl -s http://%(wpdomain)s/scripts/na-postinstall.php' % env)

    if confirm("Create child blogs?"): create_blogs()


def flush_rewrites():
    if env.flush_rewrites:
        if env.run('type wp'):
            print("Flushing WordPress Rewrites")
            with cd(env.path):
                env.run('wp rewrite flush')
        else:
            print('WP-CLI not installed. Unable to flush rewrites. ')
            print('Run `fab %(settings)s install_wp_cli` to enable rewrite flushing.' % env)


def install_wp_cli(home_dir="$HOME"):
    env.home_dir = home_dir
    env.cli = """# User Scripts
PATH=$HOME/bin:$PATH

""" % env
    # From: http://wp-cli.org/
    env.run('curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar')

    env.run('chmod +x wp-cli.phar')
    env.run('mv wp-cli.phar ~/bin/wp')
    env.run('wp --version')

    if exists("%(home_dir)s/.profile" % env) and not contains("%(home_dir)s/.profile" % env, "CLICOLOR"):
        env.run('echo "%(cli)s" >> %(home_dir)s/.profile' % env)


def enable_xmlrpc():
    """ Enable remote xmlrpc access to WordPress API (may be unsafe on production hosts) """
    env.run('echo "UPDATE wp_options SET option_value = \'1\' WHERE option_name = \'enable_xmlrpc\';" | mysql --host=%(db_host)s --user=%(db_root_user)s --password=\'%(db_root_pass)s\' %(db_name)s' % env)


def create_db():
    """ Create WordPress database """

    if not env.db_root_pass:
        env.db_root_pass = getpass("Database password: ")

    env.run('echo "CREATE DATABASE IF NOT EXISTS \\\`%(db_name)s\\\`;" | mysql --host=%(db_host)s  --user=%(db_root_user)s --password=\'%(db_root_pass)s\'' % env)


def grant_db():
    env.run('echo "GRANT ALL ON * TO \'%(db_wpuser_name)s\'@\'localhost\' IDENTIFIED BY \'%(db_wpuser_pass)s\';" | mysql --host=%(db_host)s --user=%(db_root_user)s --password=\'%(db_root_pass)s\' %(db_name)s' % env)


def snatch_db():
    """ Snatch the DB and load it locally """
    dump_db()
    get_db()
    local('fab load_db')


def load_db(dump_slug='dump'):
    """ Load WordPress database from a dump """
    dump_db('backup-%s-%s' % (env.settings, datetime.strftime(datetime.now(), format="%Y-%m-%d-%H%M")))
    env.dump_slug = dump_slug

    if not env.db_root_pass:
        env.db_root_pass = getpass("Database password: ")
    with cd(env.path):
        env.run("bzip2 -dc %(data_path)s/%(dump_slug)s.sql.bz2 | sed s@WPDEPLOYDOMAIN@%(wpdomain)s@g | mysql --host=%(db_host)s --user=%(db_root_user)s --password='%(db_root_pass)s' %(db_name)s" % env)


def dump_db(dump_slug='dump'):
    """ Dump WordPress database """

    env.dump_slug = dump_slug
    setup_folder_perms()
    if not env.db_root_pass:
        env.db_root_pass = getpass("Database password: ")
    with cd(env.path):
        env.run("mysqldump --host=%(db_host)s --user=%(db_root_user)s --password='%(db_root_pass)s' %(db_name)s | sed s@%(wpdomain)s@WPDEPLOYDOMAIN@g |bzip2 > %(data_path)s/%(dump_slug)s.sql.bz2" % env)


def backup_db():
    """ Shortcut command to create a timestamped backup """
    dump_db('backup-deployment-%s-%s' % (env.settings, datetime.strftime(datetime.now(), format="%Y-%m-%d-%H%M")))


def destroy_db():
    """ Drop WordPress database """
    if not env.db_root_pass:
        env.db_root_pass = getpass("Database password: ")

    with settings(warn_only=True):
        env.run('mysqladmin -f --host=%(db_host)s --user=%(db_root_user)s --password=\'%(db_root_pass)s\' drop %(db_name)s' % env)


def change_div_tags():
    """ Replace <div> tags in post content with <p> tags"""

    with settings(warn_only=True):
        env.run('echo "UPDATE wp_posts SET post_content = REPLACE ( post_content, \'<div\', \'<p\' ) WHERE post_type = \'post\'; UPDATE wp_posts SET post_content = REPLACE ( post_content, \'</div\', \'</p\' ) WHERE post_type = \'post\';" | mysql --host=%(db_host)s --user=%(db_root_user)s --password=\'%(db_root_pass)s\' %(db_name)s' % env)


def change_theme(theme='default'):
    """ Change WordPress theme.  Usage: `change_theme:theme_name` (ex: fab staging change_theme:twentyten) """
    env.theme = theme
    env.run('echo "UPDATE wp_options SET option_value = \'%(theme)s\' WHERE option_name IN (\'current_theme\', \'template\', \'stylesheet\');" | mysql --host=%(db_host)s --user=%(db_root_user)s --password=\'%(db_root_pass)s\' %(db_name)s' % env)


def current_theme():
    """ Returns the name of the current theme. """
    theme = env.run('mysql --host=%(db_host)s --user=%(db_root_user)s --password=\'%(db_root_pass)s\' %(db_name)s -e "SELECT option_value FROM wp_options WHERE option_name = \'template\';" -s --skip-column-names ' % env, capture=True)
    print(theme)
    return theme


def create_htaccess(htaccess_file="htaccess.template"):
    """ Create a .htaccess file based on fabconfig settings"""
    env.htaccess_file = htaccess_file
    if not exists('%(docroot_path)s/.htaccess' % env) or not contains('%(docroot_path)s/.htaccess' % env, '# BEGIN WordPress'):

        if exists("%(docroot_path)s/%(htaccess_file)s" % env):
            run('cp %(docroot_path)s/%(htaccess_file)s %(docroot_path)s/.htaccess' % env)
        else:
            print(red("No .htacces template found at %(docroot_path)s/%(htaccess_file)s" % env))


def create_wp_config():
    """ Create a wp-config.php file based on fabconfig settings"""
    if not exists("%(path)s/%(config_file)s" % env):
        print(red("No wp-config.php template found at %(path)s/%(config_file)s" % env))
    else:
        print('Looking for' + white('%(path)s/wp-config.php' % env ) + '...')
        if exists('%(path)s/wp-config.php' % env):
            if confirm('A ' + white('wp-config.php') + ' already exists. Replace it?'):
                upload_template('%(local_path)s/%(config_file)s' % env, '%(path)s/wp-config.php' % env,  env)
        else:
            print('Uploading template using %(settings)s configuration..' % env)
            upload_template('%(local_path)s/%(config_file)s' % env, '%(path)s/wp-config.php' % env,  env)



def copy_key_to_vcs(home_dir="$HOME"):
    """ Copys local public key to vcs """
    env.home_dir = home_dir
    if not exists('%(home_dir)s/.ssh/id_rsa.pub' % env, verbose=True):
        print('.ssh key directory does not exist. Generating ssh key...')
        run('ssh-keygen -t rsa -N "" -f  %(home_dir)s/.ssh/id_rsa' % env)
    key = env.run('cat %(home_dir)s/.ssh/id_rsa.pub' % env)
    if key:
        config = SafeConfigParser()
        if 'github' in env.gitrepo:
            try:
                print("Github detected.  Copying SSH key to Github.")
                config.read([os.path.join(os.getenv('HOME'), '.gitconfig')])

                from github3 import login

                g = login(config.get('github', 'user'), config.get('github', 'password'))
                g.create_key(title=env.wpdomain, key=key)

            except ImportError:
                print("Could not load github3 module.  Try running: $ pip install requirements.txt ")
            except:
                print("Your ~/.gitconfig file appears to be missing.  Please create it and try again.")

        if 'beanstalk' in env.gitrepo:
            print("Beanstalk detected.  Copying SSH key to Beanstalk.")

            # Setup beanstalk auto-deploy server
            config = SafeConfigParser()
            try:
                import beanstalk
                config.read([os.path.join(os.getenv('HOME'), '.beanstalk')])
                beanstalk.setup(
                    config.get('beanstalk', 'domain'),
                    config.get('beanstalk', 'user'),
                    config.get('beanstalk', 'password'))
                beanstalk.api.publickey.create(key, name=env.wpdomain)
            except ImportError:
                print("Could not load beanstalk module.  Try running: $ pip install -r requirements.txt ")
            except ConfigParserError:
                print("Your ~/.beanstalk file appears to be missing.  Please create it and try again. %s" % e)
            except Exception as e:
                print("Beanstalk integration failed: %s" % e)


def send_key(key_name="id_rsa", home_dir="$HOME"):
    env.home_dir = home_dir
    env.key_name = key_name
    keyfile = '/tmp/%(user)s.pub' % env
    if not exists("%(home_dir)s/.ssh" % env):
        run('mkdir -p %(home_dir)s/.ssh && chmod 700 %(home_dir)s/.ssh' % env)
    put("~/.ssh/%(key_name)s.pub" % env, keyfile)

    run('printf "\n" >> %s/.ssh/authorized_keys' % (env.home_dir))
    run("cat %s >> %s/.ssh/authorized_keys" % (keyfile, env.home_dir))
    run('printf "\n" >> %s/.ssh/authorized_keys' % (env.home_dir))

    run('rm %s' % keyfile)


def destroy_attachments():
    """ Multisite: Remove attachments """
    with cd(env.path):
        env.run('rm -rf wp-content/blogs.dir')


def reload_db(dump_slug='dump'):
    """ Destroy WordPress database and reload from latest dump"""
    destroy_db()
    create_db()
    load_db(dump_slug)


def create_blogs():
    """ Multisite: create network blogs """
    response = "Success"
    base_cmd = 'curl -s http://%(wpdomain)s/scripts/na-createblog.php' % env
    i = 0
    while "Success" in response:
        response = env.run(base_cmd + '?new_blog_index=%s' % i)
        i += 1
        print(response)
    print("Created %s blogs" % str(i - 1))


def fix_perms():
    """ Fix file permissions """
    env.sudo("chown -Rf %(user)s:%(apache_group)s %(docroot_path)s; chmod -Rf ug+rw %(docroot_path)s; chown -Rf %(apache_user)s:%(apache_group)s %(docroot_path)s/www/wp-content/uploads/; chmod -Rf ug+rw %(docroot_path)s/www/wp-content/uploads/;" % env)


def snatch_media():
    """ Sync remote media files to a local folder """
    if not env.port:
        env.port = 22
    local("rsync  -pthrvz  --rsh='ssh  -p %(port)s ' %(user)s@%(host_string)s:%(docroot_path)s/wp-content/uploads/ %(local_path)s/www/wp-content/uploads/" % env)


def send_media(path=None):
    """ Sync remote media files to a local folder """
    if path:
        env.local_path = path
    if not env.port:
        env.port = 22
    if env.fix_perms:
        fix_perms()
    local("rsync  -pthrvz  --rsh='ssh  -p %(port)s ' %(local_path)s/www/wp-content/uploads/ %(user)s@%(host_string)s:%(docroot_path)s/wp-content/uploads/" % env)


def send_media_to(target=None):
    """ Sync remote media files to another remote media folder """
    env.target = target

    with cd(env.path):
        try:
            run("fab %(target)s send_media" % env)
        except:
            print(red("Could not use fabric on %(host_string)s to send media to %(target)s" % env))


def wrap_media():
    """ Archive media attachments """
    with cd(env.path):
        env.run('tar zcf data/media.tgz wp-content/uploads/*')
    print('Wrapped up media.\n')

def unwrap_media():
    """ Unarchive media attachments """
    with cd(env.path):
        env.run('tar zxf data/media.tgz')
    print('Unwrapped media.\n')

def put_media():
    """ Put media attachments archive to remote host """
    with cd(env.path):
      check_env()
      put('%(local_path)s/data/media.tgz' % env,'%(path)s/data/media.tgz' % env)
      print('Put media on server.\n')

def get_media():
    """ Get media attachments archive from remote host """
    with lcd(env.path):
      check_env()
      get('%(path)s/data/media.tgz' % env, '%(local_path)s/data/media.tgz' % env)
      print('Got media from the server.\n')

def put_db(dump_slug='dump'):
    """ Put database dump to remote host """
    with cd(env.path):
      env.dump_slug = dump_slug
      check_env()

      put("%(local_path)s/data/%(dump_slug)s.sql.bz2" % env, '%(data_path)s/%(dump_slug)s.sql.bz2' % env)
      print('Put db on server.\n')

def get_db(dump_slug='dump'):
    """ Get database dump from remote host """
    env.dump_slug = dump_slug
    check_env()
    setup_folder_perms()

    get('%(data_path)s/%(dump_slug)s.sql.bz2' % env, "%(local_path)s/data/%(dump_slug)s.sql.bz2" % env)
    print('Got db from the server.\n')

def maintenance_down():
    """ Remove maintenance mode index.html file """
    if exists("%(path)s/index.html" % env):
        env.run('mv %(path)s/index.html %(path)s/maintenance.html' % env)
        print('Maintenance mode index.html disabled.')
    else:
        print('Maintenance mode index.html not found.  Site is not in maintenance mode.')

def maintenance_up():
    """ Setup maintenance mode index.html file """
    if exists("%(path)s/maintenance.html" % env):
        env.run('mv %(path)s/maintenance.html %(path)s/index.html' % env)
        print('Maintenance mode index.html enabled.')
    else:
        print('Warning maintenance mode maintenance.html not found. Site can not be put into maintenance mode.')


def robots_up():
    """ Setup Robots.txt to hide site from spiders/search engines """
    with cd(env.path):
        run("""echo "\nUser-agent: * \nDisallow: /" > robots.txt """)


def robots_down():
    """ Remove Robots.txt rules, allow site to be found from spiders/search engines """
    with cd(env.path):
        run("""echo "\nUser-agent: * \nDisallow: /wp-admin \nDisallow: /wp-includes \nDisallow: /wp-content/plugins \nAllow: /" > robots.txt """)


def generate_custom_archives(custom_type=None):
    if custom_type is None:
      print("Usage: `generate_custom_archives:post_type` (ex: fab generate_custom_archives:events)")
      return

    env.custom_type = custom_type
    env.theme_name = current_theme()
    with lcd("%(path)s/wp-content/themes/%(theme_name)s" % env):
      env.run('pwd')
      env.run('cp archive.php archive-%(custom_type)s.php; cp single.php single-%(custom_type)s.php' % env)


"""
Deaths, destroyers of worlds
"""
def shiva_the_destroyer():
    """
    Remove all directories, databases, etc. associated with the application.
    """
    try:
        check_env()
        env.run('rm -Rf %(path)s/* %(path)s/.*;' % env)
        destroy_db()
    except NameError, e:
        with settings(warn_only=True):
            env.run('rm .htaccess')
            env.run('rm wp-config.php')
        destroy_db()

"""
Utilities
"""


def setup_gitconfig(home_dir="$HOME"):
    """ Setup ~/.gitconfig for colors, and shortcuts """
    env.home_dir = home_dir
    if not exists("%(home_dir)s/.gitconfig" % env):
        with hide('running', 'stdout'):

            run("""echo "[user]
name = %(user)s
email = %(user)s@%(wpdomain)s

[color]
diff = auto
status = auto
branch = auto
interactive = auto
ui = true
pager = true

[alias]
ci = commit
co = checkout
st = status
undo = reset --soft HEAD^" >> %(home_dir)s/.gitconfig
""" % env)


def setup_bash_profile(home_dir="$HOME"):
    """ Setup BASH profile shortcuts, color, prompt """
    env.home_dir = home_dir
    if not exists("%(home_dir)s/.profile" % env):

        with hide('running', 'stdout'):
            if not contains("$HOME/.profile", "CLICOLOR"):
                run('echo "\nexport CLICOLOR=1 \nalias ls=\'ls -ahFG\' \nalias ll=\'ls -l\'\n" >> %(home_dir)s/.profile' % env)

            if not contains("$HOME/.profile", "export PS1"):
                if env.settings == "production":
                    env.bash_color = 1
                else:
                    env.bash_color = 2
                env.bash_prefix = env.settings.upper()
                run("""echo '\nexport PS1="\[$(tput bold)\]\[$(tput setaf %(bash_color)s)\]%(bash_prefix)s [\u@\h \W]\\$ \[$(tput sgr0)\]"' >>  %(home_dir)s/.profile """ % env)


def check_env():
    require('settings', provided_by=[production, staging])
    env.sudo = sudo
    env.run = run


def setup_folder_perms():
    """ Setup folder permissions for writeable folders """
    env.run('mkdir -p %(data_path)s' % env)
    if env.fix_perms:
        fix_perms()


def get_wordpress():
    print("Downloading and installing Wordpress...")
    with cd(env.path):
        env.run('curl -s %(wp_tarball)s | tar xzf - ' % env)
        env.run('cp -Rf wordpress/* .')
        env.run('rm -rf wordpress')
    print("Done.")


def install_plugin(name, version='latest'):
    try:
        from lxml.html import parse
        from lxml.cssselect import CSSSelector
    except ImportError:
        print("I need lxml to do this")
        exit()

    print("Looking for %s..." % name)

    url = "http://wordpress.org/extend/plugins/%s/" % name
    p = parse("%sdownload/" % url)
    sel = CSSSelector('.block-content .unmarked-list a')
    dload_elems = sel(p)

    if not dload_elems:
        print("Can't find plugin %s" % name)
        exit()

    #first is latest
    if version == 'latest':
        plugin_zip = dload_elems[0].attrib['href']
        version = dload_elems[0].text
    else:
        plugin_zip = None
        for e in dload_elems:
            if e.text == 'version':
                plugin_zip = e.attrib['href']
                break

    if not plugin_zip:
        print("Can't find plugin %s" % name)
        exit()
    else:
        print("Found version %s of %s, installing..." % (version, name) )
        with cd(env.path + "/wp-content/plugins"):
            env.run('curl -s %s -o %s.%s.zip' % (plugin_zip, name, version) )
            env.run('unzip -n %s.%s.zip' % (name, version) )

        if raw_input("Read instructions for %s? [Y|n]" % name) in ("","Y"):
            subprocess.call(['open', url])
