<?php

define('DEV_MODE', false);
define('WP_DEBUG', false);
define('SAVEQUERIES', true);
define('WPLANG', '');
define('WP_SITEURL', 'http://portlandopera.test');
define('WP_HOME', 'http://portlandopera.test');
/* define('WP_SITEURL', 'http://' . getenv('HTTP_HOST')); */
/* define('WP_HOME', 'http://' . getenv('HTTP_HOST')); */

/** DB settings */
define('DB_NAME', 'opera');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'mysql');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Mp|?zL]m2817[oLJS?Tbt3sQh5HzVC.%_s<HsBq%^+11io(M`JB~+-83LMuLBXxo');
define('SECURE_AUTH_KEY', '+I80O(mKE 2obEg | O,]k- 2@,`U`)JY.jn<2u/}=;??c_m=tm`zFgsG!0?3DX_');
define('LOGGED_IN_KEY', 'N]pf%XmS&1>&3dd`7U_*r!pq)xuDWM|S7st>1mypst(M-c{u6G #9,J -T7,+wzu');
define('NONCE_KEY', 'n+Xz4i/T$hXs+gxkBeB|OYpAk|[j4W8CP*fjTh.Q.Arih7Xq^VlNN&oga=!oc%Ii');
define('AUTH_SALT', '6k=yPV,bbxR>X[6*=h j_dcn4M~qPSVShS6uNK5Pk8Mwz9xGhL4?SY?.XpHE2JsN');
define('SECURE_AUTH_SALT', '%>3)H`: ]iBlD,}Xh-EAbHi>mYO_DW--pg5K^y+^^jO@D&w]&/*63cQ9ILw?EPLu');
define('LOGGED_IN_SALT', '3%TZ>~`wG-gg{(Dr_WD[~0&2uxP`]GXs5(Y#`FJ>f>& JY}T^NK)X&qxm,mv;U/4');
define('NONCE_SALT', '5X+gK=~LDc i>:gsPpmLCT=wsytQgfsA&<YXj^;m*`7|*A(vo-&oAc5)RX96!{;O');

$table_prefix  = 'wp_';


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
